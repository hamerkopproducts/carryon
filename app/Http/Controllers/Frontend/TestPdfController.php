<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class TestPdfController extends Controller
{
    public function index(){
        $data = ['title' => 'Welcome to carryon'];
        $pdf = PDF::loadView('pdf.order', $data);
  
        return $pdf->download('carryon' . time() . '.pdf');
    }
}
