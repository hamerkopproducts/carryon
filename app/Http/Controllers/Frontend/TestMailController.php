<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestEmail;

class TestMailController extends Controller
{
    public function index(){
        try {
            $data = ['from_name' => 'CARRYON', 'from_email' => 'noreply@carryon.com', 'subject' => 'New mail from carryon.com', 'message1' => 'This is a test1 New mail from carryon.com!', 'message2' => 'This is a test2 New mail from carryon.com!'];

			Mail::to('kamarudduja@gmail.com')->send(new TestEmail($data));
        }catch (\Exception $e) {
            print_r($e->getMessage());
        }
    }
}
