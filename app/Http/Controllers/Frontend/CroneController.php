<?php

namespace App\Http\Controllers\Frontend;

use DateTime;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\RemainderEmail;
use App\Mail\ReviewEmail;
use App\ShortLink;
use Illuminate\Support\Str;
use App\Orderlog;
use App\Mail\OrderReadyForPickup;

class CroneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = DB::select('select id, name, description from category where visibility = "YES" ');
        return view('frontend.home.index', array('categories' => $categories));
    }
    
    /**
     * Crone to update order status periodically.
     *
     * @return \Illuminate\Http\Response
     */
    public function statuschange()
    {
        $orders = Array();    
        $lowertime = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s").' -4 hours'));
        $uppertime = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s").' -3 hours'));
        $timestamp = date('Y-m-d H:i:s');
        $orders = DB::select("select `order`.`id` as orderid,`flight_time_check`.`orderdate` as orderdate from `order` join `flight_time_check` on `flight_time_check`.`orderid` = `order`.`id` where `order`.`status` = 'ORDER_CONFIRMED' and `flight_time_check`.`orderdate` > :lowertime and `flight_time_check`.`orderdate` < :uppertime ",['lowertime' => $lowertime,'uppertime' => $uppertime ]);

        if(!empty($orders)) {
            foreach ($orders as $key => $value) {
                $response = DB::table('order')->where('id', '=', $value->orderid)->update([
                    'status' => 'IN_KITCHEN',
                    'updated_at' => $timestamp
                ]);                
            }
        }
   
//        $orders = Array();          
//        $orders = DB::select("select `order`.`id` as orderid,`flight_time_check`.`orderdate` as orderdate from `order` join `flight_time_check` on `flight_time_check`.`orderid` = `order`.`id` where `order`.`status` = 'IN_KITCHEN' and `flight_time_check`.`orderdate` > :uppertime ",['uppertime' => $uppertime ]);
//        if(!empty($orders)) {
//            foreach ($orders as $key => $value) {
//                $response = DB::table('order')->where('id', '=', $value->orderid)->update([
//                    'status' => 'READY_FOR_PICKUP',
//                    'updated_at' => $timestamp
//                ]);                
//            }
//        }
    }
    
    
    public function flightgateupdate() {
            date_default_timezone_set('Asia/Dubai');
            $date = date("Y-m-d H:i:s");
            $onedayafter = date("Y-m-d H:i:s", strtotime($date. ' +24 hours'));
            $orders = DB::select("select `order`.`id` as orderid, `order`.`flight_number` as flight_number, DATE_FORMAT(`order`.`order_at`,'%Y/%c/%e') as order_at 
                            from `order` where `order`.`order_at` < :onedayafter and `order`.`gate` = '-' ",['onedayafter' => $onedayafter]);
            if (!empty($orders)) {
                foreach ($orders as $key => $value) {
                    // echo $value->order_at;
                    $fn_prefix = substr($value->flight_number, 0, 2);
                    $fn = substr($value->flight_number, 2);    
                    $handle = curl_init();

                    $url = "https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/".$fn_prefix."/".$fn."/dep/".$value->order_at."?appId=8a5a55e4&appKey=b3e7538d084234f6f83779bdfbd671ed&utc=false&airport=DXB";
                    
                    // Set the url
                    curl_setopt($handle, CURLOPT_URL, $url);
                    // Set the result output to be a string.
                    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($handle, CURLOPT_HTTPHEADER, array('DY-X-Authorization: 3b75a0bdfc7aa9d383e3f8beef0f2b623a94335c'));
                    $output = curl_exec($handle);
                    if (curl_errno($handle)) {
                        print "Error: " . curl_error($handle);
                        exit();
                    }

                    curl_close($handle);
                    $output = json_decode($output);
                    if (isset($output->flightStatuses[0]->airportResources->departureGate)) {
                        $gate = $output->flightStatuses[0]->airportResources->departureGate;
                        if($gate) {
                            $p_point = DB::table('boarding_pickup_map')->select('pickup_point')->where('gate', '=', $gate)->first();
                            $response = DB::table('order')->where('id', '=', $value->orderid)->update([
                                'gate' => $gate,
                                'updated_at' => $date,
                                'pickup_point' => $p_point->pickup_point
                            ]); 
                        }
                    } else {
                        $handle = curl_init();
                        $url = "http://aviation-edge.com/v2/public/timetable?key=3818d1-36c1c9&iataCode=DXB&type=departure&flight_iata=$value->flight_number";
                        curl_setopt($handle, CURLOPT_URL, $url);
                        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                        $output = curl_exec($handle);
                        if (curl_errno($handle)) {
                            print "Error: " . curl_error($handle);
                        }

                        curl_close($handle);
                        $output = json_decode($output);
                        if( ! isset($output->error) && isset( $output[0]->departure->gate ) && $output[0]->departure->gate != '' ) {
                            $gate = $output[0]->departure->gate;
                            $p_point = DB::table('boarding_pickup_map')->select('pickup_point')->where('gate', '=', $gate)->first();
                            $response = DB::table('order')->where('id', '=', $value->orderid)->update([
                                'gate' => $gate,
                                'updated_at' => $date,
                                'gate_updated_at' => $date,
                                'pickup_point' => $p_point->pickup_point
                            ]);
                            $orderlog = new Orderlog;
                            $orderlog->status = 'Gate Updated';
                            $orderlog->order_id = $value->orderid;
                            $orderlog->created_at = date("Y-m-d H:i:s");
                            $orderlog->updated_at = date("Y-m-d H:i:s");
                            $orderlog->save();
                        }
                        
                    }
                }
            }
            exit;
    }
    
    
    public function orderstatusupdate() {
        date_default_timezone_set('Asia/Dubai');
        $flight_slotes = DB::select("select `flight_slots`.`time_from`,`flight_slots`.`time_to`,`flight_slots`.`pickup_schedule`,`flight_slots`.`last_order`,`flight_slots`.`in_kitchen` from `flight_slots`");
        if (empty($flight_slotes)) {
            exit;
        }
        foreach ($flight_slotes as $key => $value) {
            $date = date("Y-m-d");
            $current_time = date("Y-m-d H:i:s");
            $time_from = $date." ".$value->time_from;
            $time_to = $date." ".$value->time_to;
            $pickup_schedule = $date." ".$value->pickup_schedule;
            $last_order = $date." ".$value->last_order; 
            $in_kitchen = $date." ".$value->in_kitchen;
            $kitchen_exploded = explode(":", $value->in_kitchen);
            if ($kitchen_exploded[0] >= 18) {
                $date = date("Y-m-d", strtotime(date("Y-m-d").' +1 days')); 
                $time_from = $date." ".$value->time_from;
                $time_to = $date." ".$value->time_to;
            }
            

            if ($current_time > $in_kitchen) {
                $orders = DB::select("select `order`.`id` as orderid, `order`.`flight_number` as flight_number, DATE_FORMAT(`order`.`order_at`,'%Y/%c/%e') as order_at  from `order` where `order`.`order_at` >= :time_from and `order`.`order_at` <= :time_to and `order`.`status` = 'ORDER_CONFIRMED'",['time_from' => $time_from,'time_to' => $time_to]);
                if (!empty($orders)) {
                    foreach ($orders as $okey => $ovalue) {
                            $response = DB::table('order')->where('id', '=', $ovalue->orderid)->update([
                                'status' => 'IN_KITCHEN',
                                'updated_at' => $current_time
                            ]);
                            $orderlog = new Orderlog;
                            $orderlog->status = 'In Kitchen';
                            $orderlog->order_id = $ovalue->orderid;
                            $orderlog->created_at = $current_time;
                            $orderlog->updated_at = $current_time;
                            $orderlog->save();
                    }
                }
            }
        }
        exit;
    }
    
    
    public function remaindermail() {
        date_default_timezone_set('Asia/Dubai');
        $current_time = date("Y-m-d H:i:s");
        $delivered_check_time = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s").' +1 hours'));
        $orders = DB::select("select `order`.`id` as orderid,`order`.`gate` as gate,`order`.`pickup_point` as pickup_point,`order`.`users_id` as userid, `order`.`flight_number` as flight_number, DATE_FORMAT(`order`.`order_at`,'%Y/%c/%e') as order_at from `order` where `order`.`order_at` <= :delivered_check_time and `order`.`order_at` >= :current_time and `order`.`status` = 'READY_FOR_PICKUP' and `order`.`remainder_mail_time` IS NULL",['delivered_check_time' => $delivered_check_time,'current_time' => $current_time]);
        if (!empty($orders)) {
            foreach ($orders as $okey => $ovalue) {
                        try {
                            $gateToShow = $this->findGateReminder($ovalue->pickup_point);
                            $invID = str_pad($ovalue->orderid, 5, '0', STR_PAD_LEFT);
                            $invID = "CON".$invID;
                            $user_data = DB::table('users')->select('phone_number','email','name')->where('id', '=', $ovalue->userid)->first();
                            if( $ovalue->gate == '-' || $ovalue->gate == '' ) {
                                // Destination reached but no gate. Then update gate to lift.
                                $ovalue->gate = 'Lift';
                                $maplink = url('/maps/concourseb.html?from='.$ovalue->gate.'&to='.$ovalue->pickup_point);
                            } else {
                                $first_char = strtolower( substr($ovalue->gate, 0, 1) );
                                $file = ($first_char == 'b' || $first_char == 'c') ? "concourse$first_char" : 'index';
                                $maplink = url("/maps/$file.html?from=".$ovalue->gate.'&to='.$ovalue->pickup_point);
                            }
                            // $data = ['subject' => 'REMINDER!', 'nearest_gate' => $gateToShow, 'cust_name' => $user_data->name,'order_id' => $invID,'pickup_point' => $ovalue->pickup_point,'maplink' => $maplink];
                            $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => 'EMAIL_SUBJECT_REMINDER']);
                            $subject = $subject_data[0]->template;
                            $data = ['subject' => $subject, 'nearest_gate' => $gateToShow, 'cust_name' => $user_data->name,'order_id' => $invID,'pickup_point' => $ovalue->pickup_point,'maplink' => $maplink];

                            Mail::to($user_data->email)->send(new RemainderEmail($data));
                            $response = DB::table('order')->where('id', '=', $ovalue->orderid)->update([
                                        'remainder_mail_time' => $current_time
                                        ]);
                            $orderlog = new Orderlog;
                            $orderlog->status = 'Reminder Mail';
                            $orderlog->order_id = $ovalue->orderid;
                            $orderlog->created_at = $current_time;
                            $orderlog->updated_at = $current_time;
                            $orderlog->save();
                            $template = DB::select("select template from sms_template where `type` = 'REMINDER'");
                            $message = '';
                            if( ! empty($template)) {
                                $sms_template = $template[0]->template;
                                $maplink_short = url('/m/'.$this->link_store($maplink));
                                $message = str_replace("{{ orderId }}", $invID, $sms_template);
                                $message = str_replace("{{ pickupPoint }}", $ovalue->pickup_point, $message);
                                $message = str_replace("{{ maplinkShort }}", $maplink_short, $message);
                                $message = str_replace("{{ nearest_gate }}", $gateToShow, $message);
                            }
                            // $message = 'Your order '.$invID.' is ready for pick up at '.$pickup_point.'! Find your way: '.$maplink_short;
                            $this->sendSMS($message, $user_data->phone_number);
                        } catch (\Exception $e) {
                            print_r($e->getMessage());
                        }
            }
        }
        exit;
    }
    
    public function readymail() {
        $readymail = DB::select("select * from `ready_mail` where `ready_mail`.`status` = 'NOT_SENT'");
        if (!empty($readymail)) {
            foreach ($readymail as $rkey => $rvalue) {
                $data = ['subject' => $rvalue->subject, 'cust_name' => $rvalue->cust_name,
                'nearest_gate' => $rvalue->nearest_gate, 'short_link_text' => $rvalue->short_link_text,
                'order_id' => $rvalue->order_number, 'pickup_point' => $rvalue->pickup_point, 'item_content' => $rvalue->item_content, 'maplink' => $rvalue->maplink];
                Mail::to($rvalue->email)->send(new OrderReadyForPickup($data)); 
                $response = DB::table('ready_mail')->where('id', '=', $rvalue->id)->update([
                            'status' => 'SENT'
                            ]);
            }   
        }
    }
    public function reviewmail() {
        date_default_timezone_set('Asia/Dubai');
        $current_time = date("Y-m-d H:i:s");
        //file_put_contents(getcwd() .'/resources/log.txt','reviewmailstart');
        $delivered_check_time = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s").' -24 hours'));
        $orders = DB::select("select `order`.`id` as orderid,`order`.`users_id` as userid, `order`.`flight_number` as flight_number, DATE_FORMAT(`order`.`order_at`,'%Y/%c/%e') as order_at from `order` where `order`.`delivered_at` <= :delivered_check_time and `order`.`status` = 'DELIVERED' and `order`.`review_mail_time` IS NULL",['delivered_check_time' => $delivered_check_time]);
        //file_put_contents(getcwd() .'/resources/log.txt',print_r($orders,1),FILE_APPEND);
//        echo "<pre>";
//            print_r($orders);
//            exit;
        if (!empty($orders)) {
            foreach ($orders as $okey => $ovalue) {
                        try {
                            $key = md5($this->generateCode(10));
                            DB::table('order')
                            ->where('id', $ovalue->orderid)
                            ->update([
                                'key' => $key,
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]);
                            $reviewlink = route('orderreview', ['key' => $key]);
                            $user_data = DB::table('users')->select('phone_number','email','name')->where('id', '=', $ovalue->userid)->first();
                            //$data = ['subject' => 'Please share your feedback!', 'cust_name' => $user_data->name, 'reviewlink' => $reviewlink];
                            $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => 'EMAIL_SUBJECT_REVIEW']);
                            $subject = $subject_data[0]->template;
                            $data = ['subject' => $subject, 'cust_name' => $user_data->name, 'reviewlink' => $reviewlink];

                            Mail::to($user_data->email)->send(new ReviewEmail($data));
                            $response = DB::table('order')->where('id', '=', $ovalue->orderid)->update([
                                        'review_mail_time' => $current_time
                                        ]); 
                            $orderlog = new Orderlog;
                            $orderlog->status = 'Thank Mail';
                            $orderlog->order_id = $ovalue->orderid;
                            $orderlog->created_at = $current_time;
                            $orderlog->updated_at = $current_time;
                            $orderlog->save();
                        } catch (\Exception $e) {
                            print_r($e->getMessage());
                        }
            }
        }
        exit;
    }
    
    function generateCode($limit){
        $code = '';
        for($i = 0; $i < $limit; $i++) { 
            $code .= mt_rand(0, 9); 
        }
        return substr(str_shuffle('abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789'),0,5).$code;
    }
    
    function sendSMS($message, $phonenumber) {
        $message = urlencode($message);
        preg_match_all('!\d+!', $phonenumber, $matches);
        $phone = implode('',$matches[0]);
        $url = "http://mshastra.com/sendurlcomma.aspx?user=20093644&pwd=Emirates321!&senderid=CarryonDXB&mobileno=".$phone."&msgtext=".$message."&priority=High&CountryCode=ALL";



        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        echo $curl_scraped_page;
    }
    
    public function link_store($long_link)
    {
        $input['link'] = $long_link;
        $input['code'] = Str::random(6);
   
        ShortLink::create($input);
  
        return $input['code'];
    } 
    
    function findGateReminder($gateFind) {
        $gate2Find = strtolower( $gateFind );
        if (strpos($gate2Find, 'grabbit') !== false) {
            return 'B7';
        }
        if (strpos($gate2Find, 's34') !== false) {
            return 'C36';
        }
        if (strpos($gate2Find, 'nutella') !== false) {
            return 'B28';
        }
        if (strpos($gate2Find, 'fix') !== false) {
            return 'A17';
        }
        if (strpos($gate2Find, 'tree') !== false) {
            return 'A12';
        }

        return '';
    }
    
}
