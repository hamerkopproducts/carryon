<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cartkey;
use App\Cart;
use App\Cartitems;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = DB::select('select id, name, description from category where visibility = "YES" ');
        return view('frontend.home.index', array('categories' => $categories));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $key = $request->post('key');
        $users_id = NULL;
        if( $request->session()->get('user_id') ) {
            $users_id = $request->session()->get('user_id');
        }
        $item_count = $request->post('item_count');
        $productsid = $request->post('productsid');
        if ((empty($item_count) && $item_count != 0) || empty($productsid)) {
            $returnData = Array("message" => "item count, price and product id is mandatory", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        }
        $pricedata = DB::select("select actual_price from products where `id` = :id ", ['id' => $productsid]);
        if (empty($pricedata)) {
            $returnData = Array("message" => "No product available", 'status' => FALSE);
            echo json_encode($returnData);
            exit;            
        }
        $price = $pricedata[0]->actual_price;
        if (empty($key)) {
            if ($item_count == 0) {
                $returnData = Array("message" => "Cannot add an item as zero quantity");
                echo json_encode($returnData);
                exit;                
            }
            $timestamp = date('Y-m-d H:i:s');
            $cart = new Cart;
            if (is_null($users_id)) {
                $cart->users_id = NULL;
                $cart->is_guest = 'YES';
            } else {
                $cart->users_id = $users_id;
                $cart->is_guest = 'NO';
            }
            $cart->item_count = $item_count;
            $tax_percentage = tax_percentage();
            $tax_total = (($price * $item_count) * $tax_percentage);
            $grant_total = ($price * $item_count) + $tax_total; 
            $cart->grant_total = $grant_total;
            $cart->tax_total = $tax_total;
            $cart->created_at = $timestamp;
            $cart->updated_at = $timestamp;
            $cart->sub_total = ($price * $item_count);
            $savecart = $cart->save();
            $cartid = $cart->id;
            
            $cartitems = new Cartitems();
            $cartitems->cart_id = $cartid;
            $cartitems->products_id = $productsid;
            $cartitems->item_count = $item_count;
            $cartitems->grant_total = $grant_total;
            $cartitems->tax_total = $tax_total;
            $cartitems->sub_total = ($price * $item_count);
            $cartitems->created_at = $timestamp;
            $cartitems->updated_at = $timestamp;
            $savecartitems = $cartitems->save();
            
            $cartkey = new Cartkey;
            $cartkey->cart_id = $cartid;
            $key = md5($this->generateCode(10));
            $cartkey->key = $key;
            $cartkey->created_at = $timestamp;
            $cartkey->updated_at = $timestamp;
            // $key = $cartkey->save();
            $cartkey->save();

            $request->session()->put('session_cart_key', $key);
            $request->session()->save();

            $returnData = Array('cartkey' =>$key,'message' => 'successfully added', 'status' => TRUE); 
            echo json_encode($returnData);
            return;
        } else {
            $timestamp = date('Y-m-d H:i:s');
            $cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
            if (empty($cartkey)) {
                $returnData = Array('cartkey' =>$key,"message" => "Not a valid cart key", 'status' => FALSE);
                echo json_encode($returnData);
                exit;
            } else {
                $cartid = $cartkey[0]->id;
                DB::table('cart_items')->where('cart_id', $cartid)->where('products_id', $productsid)->delete();  
                
                if ($item_count != 0){ 
                    $tax_percentage = tax_percentage();
                    $tax_total = (($price * $item_count) * $tax_percentage);
                    $grant_total = ($price * $item_count) + $tax_total; 
                    $cartitems = new Cartitems();
                    $cartitems->cart_id = $cartid;
                    $cartitems->products_id = $productsid;
                    $cartitems->item_count = $item_count;
                    $cartitems->grant_total = $grant_total;
                    $cartitems->tax_total = $tax_total;
                    $cartitems->sub_total = ($price * $item_count);
                    $cartitems->created_at = $timestamp;
                    $cartitems->updated_at = $timestamp;
                    $savecartitems = $cartitems->save();
                }
                
                $carttotaldetails = DB::select("select sum(item_count) as itemcnt,sum(grant_total) as granttotal,sum(sub_total) as subtotal,sum(tax_total) as taxtotal  from cart_items where `cart_id` = :cartid group by  cart_id", ['cartid' => $cartid]);
                if (empty($carttotaldetails)) {
                    $item_count_sum = 0;
                    $grant_total_sum = 0.00;
                    $sub_total_sum = 0.00;
                    $tax_total_sum = 0.00;                   
                } else {
                    $item_count_sum = $carttotaldetails[0]->itemcnt;
                    $grant_total_sum = $carttotaldetails[0]->granttotal;
                    $sub_total_sum = $carttotaldetails[0]->subtotal;
                    $tax_total_sum = $carttotaldetails[0]->taxtotal;                    
                }
                
                DB::table('cart')
                ->where('id', $cartid)
                ->update([
                    'item_count'       => $item_count_sum,
                    'grant_total'=> $grant_total_sum,
                    'sub_total' => $sub_total_sum,
                    'tax_total' => $tax_total_sum,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                $returnData = Array('cartkey' =>$key,'message' => 'successfully updated', 'status' => TRUE); 
                echo json_encode($returnData);
                return;
            }
        }
        $returnData = Array('cartkey' =>$key); 
        echo json_encode($returnData);
        exit;
    }
    
    function generateCode($limit){
        $code = '';
        for($i = 0; $i < $limit; $i++) { 
            $code .= mt_rand(0, 9); 
        }
        return substr(str_shuffle('abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789'),0,5).$code;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $key = $request->post('key');
        if (empty($key)) {
            $returnData = Array("message" => "Key is not passed", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        }
        $cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
        if (empty($cartkey)) {
            $returnData = Array("message" => "Not a valid key", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        } else {
            $cartid = $cartkey[0]->id;
            $result = DB::select('select cart.id as cartid, '
                    . 'cart.item_count as item_count_total,'
                    . ' cart.grant_total as grant_total_total,'
                    . ' cart.sub_total as sub_total_total,'
                    . ' cart.tax_total as tax_total_total,'
                    . ' cart_items.item_count as item_count_item_total,'
                    . ' cart_items.grant_total as grant_total_item_total,'
                    . ' cart_items.sub_total as sub_total_item_total,'
                    . ' cart_items.tax_total as tax_total_item_total,'
                    
                    . ' products.id as product_id,'      
                    . ' products.name as product_name,'
                    . 'product_images.path'

                    . ' from cart '
                    
                    . ' join cart_items on cart.id = cart_items.cart_id '
                    . ' join products on products.id = cart_items.products_id '
                    . ' join product_images on products.id = product_images.products_id '
                    . ' where product_images.type="THUMBNAIL" and cart.id = :cartid', ['cartid' => $cartid ] );

                $modifiedResult = Array(
                                        'maindata' => Array(),
                                        'eachdata' => Array(),
                                        'status' => TRUE
                                );
                foreach ($result as $key => $value) {
                    $modifiedResult['maindata'] = Array(
                                                        'item_count_total' => $value->item_count_total,
                                                        'grant_total_total' => $value->grant_total_total,
                                                        'sub_total_total' => $value->sub_total_total,
                                                        'tax_total_total' => $value->tax_total_total
                                                    );
                    $modifiedResult['eachdata'][$key] = Array(
                                                        'item_count_item_total' => $value->item_count_item_total,
                                                        'grant_total_item_total' => $value->grant_total_item_total,
                                                        'sub_total_item_total' => $value->sub_total_item_total,
                                                        'tax_total_item_total' => $value->tax_total_item_total,
                                                        'product_id' => $value->product_id,
                                                        'product_name' => $value->product_name,
                                                        'image_path' => url($value->path)
                                                    );
                }    
                echo json_encode($modifiedResult);
            exit;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $key = $request->post('key');
        $productsid = $request->post('productsid');
        if (empty($key) || empty($productsid)) {
            $returnData = Array("message" => "cart key and product id is mandatory", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        }
        $cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
        if (empty($cartkey)) {
            $returnData = Array("message" => "Not a valid key", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        } else {
            $cartid = $cartkey[0]->id;
            DB::table('cart_items')->where('cart_id', $cartid)->where('products_id', $productsid)->delete();  
            $carttotaldetails = DB::select("select sum(item_count) as itemcnt,sum(grant_total) as granttotal,sum(sub_total) as subtotal,sum(tax_total) as taxtotal  from cart_items where `cart_id` = :cartid group by  cart_id", ['cartid' => $cartid]);
            $item_count_sum = $carttotaldetails[0]->itemcnt;
            $grant_total_sum = $carttotaldetails[0]->granttotal;
            $sub_total_sum = $carttotaldetails[0]->subtotal;
            $tax_total_sum = $carttotaldetails[0]->taxtotal;

            DB::table('cart')
            ->where('id', $cartid)
            ->update([
                'item_count'       => $item_count_sum,
                'grant_total'=> $grant_total_sum,
                'sub_total' => $sub_total_sum,
                'tax_total' => $tax_total_sum,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $returnData = Array('cartkey' =>$key,'message' => 'successfully removed', 'status' => TRUE); 
            echo json_encode($returnData);
            exit;
        }
        //
    }
}
