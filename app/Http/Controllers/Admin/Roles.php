<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Roles extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:roles_view']);
    }

    function permissions_all_map() {
        $modules = $this->modules();
        $all = [];
        foreach($modules as $key => $mod) {
            $all[$key]['key'] = $mod;
            foreach(['_view', '_add_edit', '_delete'] as $item) {
                $displayVal = explode( "_", $item );
                $all[$key]['items'][] = [
                    'permission_key' => $mod.$item,
                    'display' => end( $displayVal ) == 'edit' ? 'Add/Edit' : end( $displayVal ),
                    'value' => FALSE
                ];
            }
        }

        return $all;
    }

    public function roles()
    {
        $permissions_data = user_permissions(Auth::user());
        $all_roles_in_database = Role::all();
        $permissions = $this->permissions_all_map();
        
        return view('admin.users.roles', [
            'roles' => $all_roles_in_database, 
            'user_permissions' => $permissions,
            'permissions' => $permissions_data['permissions'],
            'user_has_roles' => $permissions_data['roles'] ]);
    }

    function modules() {
        return [
            'users',
            'roles',
            'ingredients',
            'allergies',
            'categories',
            'products',
            'orders',
            'addons'
        ];
    }

    public function role_data(Request $request) {
        $role_id = $request->post('id');
        $permissions = Role::find($role_id)->getAllPermissions();
        if($permissions !== null) {
            echo json_encode(['message'=> 'Role fetched Successfully','permissions' => $permissions, 'status' => TRUE]);
        } else {
            echo json_encode(['message'=> 'Wrong', 'status' => FALSE]);
        }

        return;
    }

    public function role_add(Request $request) {
        $role_name = $request->post('role');
        $existCheck = Role::where('name', '=', $role_name)->first();
        if ($existCheck !== null) {
            echo json_encode(['message'=> 'Already exists', 'status' => FALSE]);

            return;
        }

        $role = Role::create(['name' => $role_name]);

        $modules = $this->modules();
        foreach($modules as $mod) {
            foreach(['_view', '_add_edit', '_delete'] as $item) {
                
                $permission = $mod.$item;
                if( $request->post($permission) == 'on' ) {
                    $permissionItem = Permission::where('name', '=', $permission)->first();
                    if($permissionItem === null) {
                        $permissionItem = Permission::create(['name' => $permission]);
                    }
                    $permissionItem->assignRole($role);
                }

            }
        }
        echo json_encode(['message'=> 'Role Added Successfully', 'status' => TRUE]);

        return;
    }

    function role_edit(Request $request) {
        $role_id = $request->post('role_id');
        $role_name = $request->post('role');
        $exist = Role::where('name', '=', $role_name)->where('id', '!=', $role_id)->first();
        if ($exist !== null) {
            echo json_encode(['message'=> 'Role already exists', 'status' => FALSE]);

            return;
        }

        $role = Role::where('id', '=', $role_id)->first();
        if ($role === null) {
            echo json_encode(['message'=> 'Not found', 'status' => FALSE]);

            return;
        }

        $role->name =  $role_name;
        $role->save();

        $modules = $this->modules();
        foreach($modules as $mod) {
            foreach(['_view', '_add_edit', '_delete'] as $item) {
                
                $permission = $mod.$item;
                if( $request->post($permission) == 'on' ) {
                    $permissionItem = Permission::where('name', '=', $permission)->first();
                    if($permissionItem === null) {
                        $permissionItem = Permission::create(['name' => $permission]);
                    }
                    $permissionItem->assignRole($role);
                } else {
                    $permissionItem = Permission::where('name', '=', $permission)->first();
                    if($permissionItem === null) {
                        $permissionItem = Permission::create(['name' => $permission]);
                    }
                    $permissionItem->removeRole($role);
                }

            }
        }
        echo json_encode(['message'=> 'Role Added Successfully', 'status' => TRUE]);

        return;
    }

}
