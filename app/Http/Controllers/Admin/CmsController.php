<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Cms;
use App\Settings;

class CmsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $permissions_data = user_permissions(Auth::user());
        return view('admin.Cms.Cms', array('cmsdetails' => $this->fetchCmsdetails(),'permissions' => $permissions_data['permissions'], 'user_has_roles' => $permissions_data['roles'] ));
    }

    public function section() {
        $permissions_data = user_permissions(Auth::user());
        return view('admin.Cms.Cms', array('cmsdetails' => $this->fetchCmsSectiondetails(),'permissions' => $permissions_data['permissions'], 'user_has_roles' => $permissions_data['roles'] ));
    }   
    
    public function socialmedia() {
        $permissions_data = user_permissions(Auth::user());
        return view('admin.Cms.Socialmedia', array('cmsdetails' => $this->fetchCmsSocialmediadetails(),'permissions' => $permissions_data['permissions'], 'user_has_roles' => $permissions_data['roles'] ));
    }     
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function emailtemplate($type) {
        $content = '';
        $title = '';
        if ($type == 'readyforpickup') {
            $subject_type = 'EMAIL_SUBJECT_READYFORPICKUP';
            $title = 'Ready For Pickup';
            $content = file_get_contents(getcwd() .'/resources/views/emails/orderreadyforpickup.blade.php');
        }
        
        if ($type == 'welcomemail') {
            $subject_type = 'EMAIL_SUBJECT_WELCOMEMAIL';
            $title = 'Welcome Mail';
            $content = file_get_contents(getcwd() .'/resources/views/emails/test.blade.php');
        }
        
        if ($type == 'cancelmail') {
            $subject_type = 'EMAIL_SUBJECT_CANCELMAIL';
            $title = 'Cancel Mail';
            $content = file_get_contents(getcwd() .'/resources/views/emails/cancelmail.blade.php');
        }
        
        if ($type == 'orderplace') {
            $subject_type = 'EMAIL_SUBJECT_ORDERPLACE';
            $title = 'Order Place';
            $content = file_get_contents(getcwd() .'/resources/views/emails/ordermail.blade.php');
        }
        
        if ($type == 'review') {
            $subject_type = 'EMAIL_SUBJECT_REVIEW';
            $title = 'Review after 24 hours';
            $content = file_get_contents(getcwd() .'/resources/views/emails/reviewmail.blade.php');
        }
        
        if ($type == 'reminder') {
            $subject_type = 'EMAIL_SUBJECT_REMINDER';
            $title = 'Reminder';
            $content = file_get_contents(getcwd() .'/resources/views/emails/remaindermail.blade.php');
        }
        $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => $subject_type]);
        $subject = $subject_data[0]->template;
        $permissions_data = user_permissions(Auth::user());
        return view('admin.Cms.Email', array('cmsdetails' => $this->fetchCmsdetails(),'permissions' => $permissions_data['permissions'], 'user_has_roles' => $permissions_data['roles'],'content' => $content, 'emailtype' => $type, 'title' => $title, 'subject' => $subject));
    }

    public function template_add(Request $request) {
        $html_content = $request->post('html_content');
        $emailtype = $request->post('emailtype'); 
        $subject = $request->post('subject');
        if ($emailtype == 'readyforpickup') {
            $subject_type = 'EMAIL_SUBJECT_READYFORPICKUP';            
            $content = file_put_contents(getcwd() .'/resources/views/emails/orderreadyforpickup.blade.php',$html_content);          
        }
        
        if ($emailtype == 'welcomemail') {
            $subject_type = 'EMAIL_SUBJECT_WELCOMEMAIL';
            $content = file_put_contents(getcwd() .'/resources/views/emails/test.blade.php',$html_content);          
        }
        
        if ($emailtype == 'cancelmail') {
            $subject_type = 'EMAIL_SUBJECT_CANCELMAIL';
            $content = file_put_contents(getcwd() .'/resources/views/emails/cancelmail.blade.php',$html_content);          
        }

        if ($emailtype == 'orderplace') {
            $subject_type = 'EMAIL_SUBJECT_ORDERPLACE';
            $content = file_put_contents(getcwd() .'/resources/views/emails/ordermail.blade.php',$html_content);          
        }
        
        if ($emailtype == 'review') {
            $subject_type = 'EMAIL_SUBJECT_REVIEW';
            $content = file_put_contents(getcwd() .'/resources/views/emails/reviewmail.blade.php',$html_content);          
        }
        
        if ($emailtype == 'reminder') {
            $subject_type = 'EMAIL_SUBJECT_REMINDER';
            $content = file_put_contents(getcwd() .'/resources/views/emails/remaindermail.blade.php',$html_content);          
        }
        $response = DB::table('sms_template')->where('type', '=', $subject_type)->update([
                'template' => $subject
            ]);           

//        $request->session()->flash('status', 'Email template updated successfully');
//        return redirect('/admin/email/'.$emailtype);
        echo json_encode(['message' => 'Email template edited Successfully', 'status' => TRUE]);
        return;
    }
    
    public function cms_add(Request $request) {
        $url_key = $request->post('url_key');
        $page_title = $request->post('page_title');
        $html_content = $request->post('html_content');
        $visibility = $request->post('visibility');
        $created_at = date('Y-m-d H:i:s');
        $existCheck = Cms::where('url_key', '=', $url_key)->first();
        if (!$url_key || !$page_title || !$html_content || !$visibility || !$created_at) {
            echo json_encode(['message' => 'Provide all fields', 'status' => FALSE]);

            return;
        }

        if ($existCheck !== null) {
            echo json_encode(['message' => 'Url key already exists', 'status' => FALSE]);

            return;
        }

        $cms = Cms::create(['url_key' => $url_key, 'page_title' => $page_title,
                    'html_content' => $html_content, 'visibility' => $visibility, 'created_at' => $created_at]);

        if ($cms) {
            echo json_encode(['message' => 'Cms Added Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }

    public function cms_edit(Request $request) {                                                                                                                                                                                                                                                                          
        $cms_id = $request->post('cms_id');
        $url_key = $request->post('url_key');
        $page_title = $request->post('page_title');
        $html_content = $request->post('html_content');
        $visibility = $request->post('visibility');
        $updated_at = date('Y-m-d H:i:s');
        if (!$url_key || !$page_title || !$html_content || !$visibility || !$updated_at) {
            echo json_encode(['message' => 'Provide all fields', 'status' => FALSE]);

            return;
        }

        $existCheck = Cms::where('url_key', '=', $url_key)->where('id', '!=', $cms_id)->first();
        if ($existCheck !== null) {
            echo json_encode(['message' => 'Url key already exists', 'status' => FALSE]);

            return;
        }

        $cms = Cms::where('id', '=', $cms_id)->first();
        $cms->url_key = $url_key;
        $cms->page_title = $page_title;
        $cms->html_content = $html_content;
        $cms->visibility = $visibility;
        $cms->updated_at = date('Y-m-d H:i:s');
        
        $cms->save();

        if ($cms) {
            echo json_encode(['message' => 'CMS Updated Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }
    
    public function socialmedia_edit(Request $request) {                                                                                                                                                                                                                                                                          
        $cms_id = $request->post('cms_id');
        $social_media_link = $request->post('social_media_link');
        $visibility = $request->post('visibility');
        $position = $request->post('position');
        $updated_at = date('Y-m-d H:i:s');
        if (!$visibility || !$updated_at || !$position) {
            echo json_encode(['message' => 'Provide all fields', 'status' => FALSE]);

            return;
        }


        $cms = Cms::where('id', '=', $cms_id)->first();
        $cms->social_media_link = $social_media_link;
        $cms->position = $position;
        $cms->visibility = $visibility;
        $cms->updated_at = date('Y-m-d H:i:s');
        
        $cms->save();

        if ($cms) {
            echo json_encode(['message' => 'CMS Updated Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }    
    
    
    
    public function smstemplate($type) {
        $content = '';
        $title = '';
        if ($type == 'ORDER_PLACE') {
            $template = DB::select("select template from sms_template where `type` = 'ORDER_PLACE'");
            $content = $template[0]->template;
            $title = "Order Place";
        }
        
        if ($type == 'READY_FOR_PICKUP') {
            $template = DB::select("select template from sms_template where `type` = 'READY_FOR_PICKUP'");
            $content = $template[0]->template;
            $title = "Ready For Pickup";
        }
        
        if ($type == 'REMINDER') {
            $template = DB::select("select template from sms_template where `type` = 'REMINDER'");
            $content = $template[0]->template;
            $title = "Reminder";
        }
        
        $permissions_data = user_permissions(Auth::user());
        return view('admin.Cms.Sms', array('permissions' => $permissions_data['permissions'], 'user_has_roles' => $permissions_data['roles'],'content' => $content, 'smstype' => $type, 'title' => $title));
    }
    
    public function smstemplate_add(Request $request) {
        $html_content = $request->post('html_content');
        $smstype = $request->post('smstype'); 
        $response = DB::table('sms_template')->where('type', '=', $smstype)->update([
                'template' => $html_content
            ]);           

        echo json_encode(['message' => 'SMS templated edited Successfully', 'status' => TRUE]);
        return;
    }

    public function cms_data(Request $request) {
        $cms_id = $request->post('id');
        $cms = Cms::select('url_key', 'page_title', 'html_content', 'visibility')->where('id', '=', $cms_id)->first();
        if ($cms !== null) {
            echo json_encode(['message' => 'Cms fetched Successfully', 'cms' => $cms, 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Wrong data', 'status' => FALSE]);
        }

        return;
    }
    
    public function socialmedia_data(Request $request) {
        $cms_id = $request->post('id');
        $cms = Cms::select('social_media_name','social_media_link', 'visibility', 'position')->where('id', '=', $cms_id)->first();
        if ($cms !== null) {
            echo json_encode(['message' => 'Cms fetched Successfully', 'cms' => $cms, 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Wrong data', 'status' => FALSE]);
        }

        return;
    }    
    
    public function cms_delete(Request $request) {                                                                                                                                                                                                                                                                          
        $cms_id = $request->post('id');
//        $cms = Cms::where('id', '=', $cms_id)->first();
//        $cms->delete();
        $cms = Cms::where('id', '=', $cms_id)->first();
        $cms->deleted_at = date('Y-m-d H:i:s');
        $cms->save();
        
        if ($cms) {
            echo json_encode(['message' => 'Deleted Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }    
    

    // Getting category details
    private function fetchCmsdetails() {
        $cms = DB::table('cms')
                ->select('id', 'url_key', 'html_content', 'page_title', 'meta_data', 'meta_description', 'meta_keywords', 'visibility','short_code')
                ->where('short_code', '=' , NULL)
                ->where('deleted_at', '=' , NULL)
                ->orderBy('created_at', 'desc')
                ->get();
        //->paginate(10);
        return $cms;
    }
    // Getting category details
    private function fetchCmsSectiondetails() {
        $cms = DB::table('cms')
                ->select('id', 'url_key', 'html_content', 'page_title', 'meta_data', 'meta_description', 'meta_keywords', 'visibility','short_code')
                ->where('short_code', '!=' , NULL)
                ->where('short_code', '!=' , 'socialmedia')
                ->where('deleted_at', '=' , NULL)
                ->orderBy('created_at', 'desc')
                ->get();
        //->paginate(10);
        return $cms;
    }
    // Getting category details
    private function fetchCmsSocialmediadetails() {
        $cms = DB::table('cms')
                ->select('id', 'social_media_name', 'social_media_link', 'visibility','short_code','position')
                ->where('short_code', '=' , 'socialmedia')
                ->where('deleted_at', '=' , NULL)
                ->orderBy('created_at', 'desc')
                ->get();
        //->paginate(10);
        return $cms;
    }     
    
    
    public function settings_edit(Request $request) {
        $settings_arr = array();
        $settings_arr = $request->post();
       // echo '<pre>'; print_r($settings_arr); exit;
        foreach($settings_arr as $key => $val){
            if($key != '_token'){
                $settings = Settings::where('key', '=', $key)->first();
                $settings->value = ($val !='')?$val:0;
                $settings->updated_at = date('Y-m-d H:i:s');
                $settings->save();
            }
        }

        if ($settings) {
            
            $request->session()->flash('status', 'Settings updated successfully');
        } else {
            $request->session()->flash('status', 'Provide all the feilds');
        }

        return redirect(route('settings_data'));
    }
    
    public function settings_data(Request $request) {
        
       
        $settings = Settings::select('id', 'key', 'value', 'display_name')->get();
        return view('admin.Cms.Settings', array('settings' => $settings));
    }

}
