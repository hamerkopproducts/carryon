<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Allergiesfilter;
use Validator;
use Illuminate\Support\Facades\Auth;

class AllergiesFliterController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $permissions_data = user_permissions(Auth::user());
          return view('admin.AllergiesFliter.AllergiesFliter', array('allergiesfliter' => $this->fetchAllergiesfliter(),
                    'permissions' => $permissions_data['permissions'],
                    'user_has_roles' => $permissions_data['roles']));
    }

    // Getting AllergiesFliter list
    private function fetchAllergiesfliter(){
        $allergies = DB::table('allergies_filter')
                        ->select('id','name','description','visibility','position','created_at','updated_at','deleted_at')
                        ->orderBy('created_at', 'desc')
                        ->get();
         return $allergies;
    }


    public function create(Request $request)
    {  
       return view('admin.AllergiesFliter.AllergiesFliterCreate');
    }

    public function add(Request $request)
    {
                   
            $validatedData = $request->validate([
                'name' => 'required',
                'description' => 'required',
                'visibility' => 'required',
                 ]);
            $allerId = '';    
            $timestamp = date('Y-m-d H:i:s');
            $allergies = new Allergiesfilter;
            $allergies->name = $request->name;
            $allergies->description = $request->description;
            $allergies->visibility = $request->visibility;
            $allergies->position = $request->position;
            $allergies->image_path = ''; //$request->image;
            $allergies->created_at = $timestamp;
            $allergies->updated_at = $timestamp;
            $save = $allergies->save();
            $allerId = $allergies->id;
            if($request->image) {
                $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/frontend/allergyfliter'), $input['image']);

                DB::table('allergies_filter')
                    ->where('id', $allerId)
                    ->update([
                        'image_path' => 'images/frontend/allergyfliter/' . $input['image']
                    ]);
            }
            if ($save) $request->session()->flash('status', 'Allergies fliter created successfully');
            return redirect('/admin/allergies_fliter');
        }

        public function edit($id)
        {
            //echo  $id;
            $allergies = DB::table('allergies_filter')
                    ->select('id', 'name', 'description', 'visibility', 'image_path','position' )
                    ->where([
                        ['id', '=', $id],
                    ])
                    ->get();
            return view('admin.AllergiesFliter.AllergiesFliterEdit', array('allergies' => $allergies));
        }

        public function update(Request $request){
            
            $id = $request->aller_id;
                if($request->image_path == '') {
                    DB::table('allergies_filter')
                   ->where('id', $id)
                   ->update([
                       'image_path' => ''
                   ]);                   
                }
                if($request->image) {
                $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/frontend/allergyfliter'), $input['image']);
                DB::table('allergies_filter')
                ->where('id', $id)
                ->update([
                    'image_path' => 'images/frontend/allergyfliter/' . $input['image']
                ]);
            
            }
          $allergie=  DB::table('allergies_filter')
                ->where('id', $id)
                ->update([
                    'name'       => $request->post('name'),
                    'description'=> $request->post('description'),
                    'visibility' => $request->post('visibility'),
                    'position' => $request->post('position'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                if ( $allergie) $request->session()->flash('status', 'Allergies fliter updated successfully');
                return redirect('/admin/allergies_fliter');
        }


        public function delete ($id)
        {
            //echo $id;
            DB::table('allergies_filter')->where('id', $id)->delete();
            return redirect('/admin/allergies_fliter');
        }
            
    } 
   

