<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DatePeriod;
use DateTime;
use DateInterval;

class ReportsController extends Controller
{
    /**
     * Displaying Reports View
     * 
     */
    public function index(){
        $categoriesdrop = DB::table('category')
                    ->select('id', 'name')
                    ->where('deleted_at', NULL)
                    ->orderBy('position', 'asc')
                    ->get();
        $diets = DB::table('preferenceoptions')
                    ->select('id', 'name')
                    ->where('deleted_at', NULL)
                    ->orderBy('created_at', 'desc')
                    ->where('type', 'Diet')
                    ->get();
        $categories = DB::table('order')
                    ->select('category.id as id',DB::raw('sum(order_items.grant_total) as total'), 'category.name as name')
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                    ->join('category', 'category.id', '=', 'product_category.category_id')
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '=', date('Y-m-d'))
                    ->groupBy('category.id')
                    ->get();
                    //->paginate(10);
        $all_categories = json_decode(json_encode($categories), true);
        $total_sale = array_sum(array_column($all_categories, 'total'));
 
        usort($all_categories, function ($a, $b) { return $b['total'] - $a['total']; });
        
        $topcategories = array_slice($all_categories,0,5);
        
        $preferenceoptions = DB::table('order')
                    ->select('preferenceoptions.id as id',DB::raw('sum(order_items.grant_total) as total'), 'preferenceoptions.name as name')
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                    ->join('preferenceoptions', 'preferenceoptions.id', '=', 'product_preferenceoptions.preferenceoptions_id')
                    ->where('order.status', 'DELIVERED')
                    ->where('product_preferenceoptions.type', 'Diet')
                    ->whereDate('order.order_at', '=', date('Y-m-d'))
                    ->groupBy('preferenceoptions.id')
                    ->get();
        $all_diets = json_decode(json_encode($preferenceoptions), true);
 
        usort($all_diets, function ($a, $b) { return $b['total'] - $a['total']; });
        
        $topdiets = array_slice($all_diets,0,5);
        return view('admin.Reports.reports',
                    array(
                        'categories' => $categoriesdrop,// For the categories dropdown list
                        'diets' => $diets, // For the diet dropdown list
                        'top_categories' => $topcategories, // For the top 5 categories
                        'top_diets' => $topdiets, // For the top 5 diets
                        'all_categories' => $all_categories, // For all the categories, this we can use in pie graph
                        'all_diets' => $all_diets, // For all the diets, this we can use in pie graph
                        'tatal_sale' => $total_sale // Total sale
                    )
                );
    }
    
    public function categories(){
        $categories = DB::table('category')
                    ->select('id', 'name')
                    ->where('deleted_at', NULL)
                    ->orderBy('position', 'asc')
                    ->get();
                    //->paginate(10);
        
        echo json_encode(['message'=> 'Categories list fetched', 'categories' => $categories, 'status' => TRUE]);
        return;
    }
    
    public function diets(){
        $diets = DB::table('preferenceoptions')
                    ->select('id', 'name')
                    ->where('deleted_at', NULL)
                    ->orderBy('created_at', 'desc')
                    ->where('type', 'Diet')
                    ->get();
                    //->paginate(10);
 
        echo json_encode(['message'=> 'Diets list fetched', 'diets' => $diets, 'status' => TRUE]);
        return;
    }
    
    public function salestab($date_from,$date_to,$cat,$diet){
        $repeated_users = DB::table('order')
                    ->select(DB::raw('count(order.id) as total'))
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('order.users_id')
                    ->get();
        $repeated_users = json_decode(json_encode($repeated_users), true);
        $repeated_users_count = 0;
        $users_count = 0;
        if (!empty($repeated_users)) {
            foreach ($repeated_users as $key => $value) {
                if ($value['total'] > 1) {
                   $repeated_users_count += 1; 
                }
                $users_count += 1;
            }
        }
        
        $products = DB::table('order')
                    ->select('products.id as id',DB::raw('sum(order_items.grant_total) as value'), 'products.name as label')
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('products', 'products.id', '=', 'order_items.products_id')
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('products.id')
                    ->get();
                    //->paginate(10);
        $all_products = json_decode(json_encode($products), true);
        usort($all_products, function ($a, $b) { return $b['value'] - $a['value']; });
        
        
        $categories = DB::table('order')
                    ->select('category.id as id',DB::raw('sum(order_items.grant_total) as value'), 'category.name as label')
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                    ->join('category', 'category.id', '=', 'product_category.category_id')
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('category.id')
                    ->get();
                    //->paginate(10);
        $all_categories = json_decode(json_encode($categories), true);
 
        usort($all_categories, function ($a, $b) { return $b['value'] - $a['value']; });
        
        $topcategories = array_slice($all_categories,0,5);
        
        
        // Applying categories filter
        $cats = json_decode($cat);
        $all_cat = $all_categories; 
        if (!empty($all_categories) && !empty($cats)) {
            $all_cat = array();
            foreach ($all_categories as $key => $value) {
                if (in_array($value['id'], $cats)) {
                  $all_cat[] = $value;  
                }
            }
        }
        
        $all_filt_cat_final = $all_cat;
       
        $preferenceoptions = DB::table('order')
                    ->select('preferenceoptions.id as id',DB::raw('sum(order_items.grant_total) as value'), 'preferenceoptions.name as label')
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                    ->join('preferenceoptions', 'preferenceoptions.id', '=', 'product_preferenceoptions.preferenceoptions_id')
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->where('product_preferenceoptions.type', 'Diet')
                    ->groupBy('preferenceoptions.id')
                    ->get();
        $all_diets = json_decode(json_encode($preferenceoptions), true);
 
        usort($all_diets, function ($a, $b) { return $b['value'] - $a['value']; });
        
        $topdiets = array_slice($all_diets,0,5);
        
        // Applying diets filter
        $diets = json_decode($diet);
        $all_diet = $all_diets; 
        if (!empty($all_diets) && !empty($diets)) {
            $all_diet = array();
            foreach ($all_diets as $key => $value) {
                if (in_array($value['id'], $diets)) {
                  $all_diet[] = $value;  
                }
            }
        } 
        $all_filt_diet_final =  $all_diet;
        $flag = false;
        if (!empty($cats) || !empty($diets)) {
            if (!empty($cats) && !empty($diets)) { 
             $diets_filtered = DB::table('order')
                         ->select('preferenceoptions.id as id','order_items.order_id as oid','order_items.products_id as pid','product_category.category_id as crid','order_items.grant_total as value', 'preferenceoptions.name as label')
                         ->join('order_items', 'order.id', '=', 'order_items.order_id')
                         ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                         ->join('preferenceoptions', 'preferenceoptions.id', '=', 'product_preferenceoptions.preferenceoptions_id')
                         ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                         ->whereIn('product_category.category_id', $cats)
                         ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                         ->where('order.status', 'DELIVERED')
                         ->whereDate('order.order_at', '>=', $date_from)
                         ->whereDate('order.order_at', '<=', $date_to)
                         ->where('product_preferenceoptions.type', 'Diet')
                         ->get();
             $flag = true;
            } else if (!empty($cats)){
              $diets_filtered = DB::table('order')
                         ->select('preferenceoptions.id as id','order_items.order_id as oid','order_items.products_id as pid','product_category.category_id as crid','order_items.grant_total as value', 'preferenceoptions.name as label')
                         ->join('order_items', 'order.id', '=', 'order_items.order_id')
                         ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                         ->join('preferenceoptions', 'preferenceoptions.id', '=', 'product_preferenceoptions.preferenceoptions_id')
                         ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                         ->whereIn('product_category.category_id', $cats)
                         ->where('order.status', 'DELIVERED')
                         ->whereDate('order.order_at', '>=', $date_from)
                         ->whereDate('order.order_at', '<=', $date_to)
                         ->where('product_preferenceoptions.type', 'Diet')
                         ->get();
              $flag = true;
            }

            if ($flag) {
                $all_diet = json_decode(json_encode($diets_filtered), true);
                $all_filt_diet_final = array();
                if (!empty($all_diet)) {
                    $pre_all_diet = array();
                    foreach ($all_diet as $key => $value) {
                        if (!isset($checkd[$value['id']."_".$value['oid']."_".$value['pid']])) {
                            $checkd[$value['id']."_".$value['oid']."_".$value['pid']] = true;
                            $pre_all_diet[] = array("id" => $value['id'], "value" => $value['value'], "label" => $value['label']); 
                        }
                    }
                    $all_filt_diet = array();
                    $sumd = array();
                    foreach ($pre_all_diet as $key => $value) {
                        if (!isset($sumd[$value["id"]])) {
                            $sumd[$value["id"]] = 0;
                        }
                        $sumd[$value["id"]] += $value["value"];
                        $sumd[$value["id"]] = $sumd[$value["id"]].".00";
                        $all_filt_diet[$value["id"]] = array("id" => $value['id'], "value" => $sumd[$value["id"]], "label" => $value['label']);
                    }
                    foreach ($all_filt_diet as $key => $value) {
                        $all_filt_diet_final[] = $value;
                    }
                } else {
                    $all_filt_diet_final = array();
                }
            }
        }
 

        
        $flag = false;
        if (!empty($cats) || !empty($diets)) {
            if (!empty($cats) && !empty($diets)) { 
             $categories_filtered = DB::table('order')
                         ->select('category.id as id','order_items.order_id as oid','order_items.products_id as pid','product_preferenceoptions.preferenceoptions_id as prid','order_items.grant_total as value', 'category.name as label')
                         ->join('order_items', 'order.id', '=', 'order_items.order_id')
                         ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                         ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                         ->join('category', 'category.id', '=', 'product_category.category_id')
                         ->whereIn('product_category.category_id', $cats)
                         ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                         ->where('order.status', 'DELIVERED')
                         ->whereDate('order.order_at', '>=', $date_from)
                         ->whereDate('order.order_at', '<=', $date_to)
                         ->get();
             $flag = true;
            } else if (!empty($diets)){
              $categories_filtered = DB::table('order')
                         ->select('category.id as id','order_items.order_id as oid','order_items.products_id as pid','product_preferenceoptions.preferenceoptions_id as prid','order_items.grant_total as value', 'category.name as label')
                         ->join('order_items', 'order.id', '=', 'order_items.order_id')
                         ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                         ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                         ->join('category', 'category.id', '=', 'product_category.category_id')
                         ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                         ->where('order.status', 'DELIVERED')
                         ->whereDate('order.order_at', '>=', $date_from)
                         ->whereDate('order.order_at', '<=', $date_to)
                         ->get();
              $flag = true;
            }

            if ($flag) {
                $all_cat = json_decode(json_encode($categories_filtered), true);
                $all_filt_cat_final = array();
                if (!empty($all_cat)) {
                    $pre_all_cat = array();
                    foreach ($all_cat as $key => $value) {
                        if (!isset($check[$value['id']."_".$value['oid']."_".$value['pid']])) {
                            $check[$value['id']."_".$value['oid']."_".$value['pid']] = true;
                            $pre_all_cat[] = array("id" => $value['id'], "value" => $value['value'], "label" => $value['label']); 
                        }
                    }
                    $all_filt_cat = array();
                    $sum = array();
                    foreach ($pre_all_cat as $key => $value) {
                        if (!isset($sum[$value["id"]])) {
                            $sum[$value["id"]] = 0;
                        }
                        $sum[$value["id"]] += $value["value"];
                        $sum[$value["id"]] = $sum[$value["id"]].".00";
                        $all_filt_cat[$value["id"]] = array("id" => $value['id'], "value" => $sum[$value["id"]], "label" => $value['label']);
                    }
                    foreach ($all_filt_cat as $key => $value) {
                        $all_filt_cat_final[] = $value;
                    }
                } else {
                    $all_filt_cat_final = array();
                }
            }
        }
        
        
        

         $flag = false;
        if (!empty($cats) || !empty($diets)) {
            if (!empty($cats) && !empty($diets)) { 
                $products_filtered = DB::table('order')
                            ->select('products.id as id','order_items.order_id as oid','order_items.products_id as pid', 'order_items.grant_total as value', 'products.name as label')
                            ->join('order_items', 'order.id', '=', 'order_items.order_id')
                            ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                            ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                            ->join('products', 'products.id', '=', 'order_items.products_id')
                            ->whereIn('product_category.category_id', $cats)
                            ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                            ->where('order.status', 'DELIVERED')
                            ->whereDate('order.order_at', '>=', $date_from)
                            ->whereDate('order.order_at', '<=', $date_to)
                            ->get();
                 $flag = true;
            } else if (!empty($diets)){
                $products_filtered = DB::table('order')
                            ->select('products.id as id','order_items.order_id as oid','order_items.products_id as pid', 'order_items.grant_total as value', 'products.name as label')
                            ->join('order_items', 'order.id', '=', 'order_items.order_id')
                            ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                            ->join('products', 'products.id', '=', 'order_items.products_id')
                            ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                            ->where('order.status', 'DELIVERED')
                            ->whereDate('order.order_at', '>=', $date_from)
                            ->whereDate('order.order_at', '<=', $date_to)
                            ->get();
                 $flag = true;
            } else if (!empty($cats)){
                $products_filtered = DB::table('order')
                            ->select('products.id as id','order_items.order_id as oid','order_items.products_id as pid', 'order_items.grant_total as value', 'products.name as label')
                            ->join('order_items', 'order.id', '=', 'order_items.order_id')
                            ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                            ->join('products', 'products.id', '=', 'order_items.products_id')
                            ->whereIn('product_category.category_id', $cats)
                            ->where('order.status', 'DELIVERED')
                            ->whereDate('order.order_at', '>=', $date_from)
                            ->whereDate('order.order_at', '<=', $date_to)
                            ->get();
                 $flag = true;
            }

            if ($flag) {
                $all_prod = json_decode(json_encode($products_filtered), true);
                $all_products = array();
                if (!empty($all_prod)) {
                    $pre_all_prod = array();
                    foreach ($all_prod as $key => $value) {
                        if (!isset($checkp[$value['id']."_".$value['oid']."_".$value['pid']])) {
                            $checkp[$value['id']."_".$value['oid']."_".$value['pid']] = true;
                            $pre_all_prod[] = array("id" => $value['id'], "value" => $value['value'], "label" => $value['label']); 
                        }
                    }
                    $all_filt_prod = array();
                    $sump = array();
                    foreach ($pre_all_prod as $key => $value) {
                        if (!isset($sump[$value["id"]])) {
                            $sump[$value["id"]] = 0;
                        }
                        $sump[$value["id"]] += $value["value"];
                        $sump[$value["id"]] = $sump[$value["id"]].".00";
                        $all_filt_prod[$value["id"]] = array("id" => $value['id'], "value" => $sump[$value["id"]], "label" => $value['label']);
                    }
                    foreach ($all_filt_prod as $key => $value) {
                        $all_products[] = $value;
                    }
                } else {
                    $all_products = array();
                }
            }
            usort($all_products, function ($a, $b) { return $b['value'] - $a['value']; });
        }
        $total_sale = array_sum(array_column($all_products, 'value'));
        $topproducts = array_slice($all_products,0,5);
        if (!empty($topproducts)) {
            foreach ($topproducts as $key => $value) {
                $topproducts[$key]['value'] = number_format($topproducts[$key]['value'],2); 
            }
        } 
        
        if (!empty($topdiets)) {
            foreach ($topdiets as $key => $value) {
                $topdiets[$key]['value'] = number_format($topdiets[$key]['value'],2); 
            }
        }
        
        if (!empty($topcategories)) {
            foreach ($topcategories as $key => $value) {
                $topcategories[$key]['value'] = number_format($topcategories[$key]['value'],2); 
            }
        }
//        echo "<pre>";
//            print_r($topproducts);
//            exit;
        
        // $total_sale = 1234.56;
        $total_sale = number_format($total_sale,2);
        
        // Note the following
        // $topcategories, // For the top 5 categories
        // $topdiets, // For the top 5 diets
        // $topproducts, // For the top 5 products
        // $all_cat, // For all the categories, this we can use in pie graph, categories filter applied
        // $all_diet // For all the diets, this we can use in pie graph, diets filter applied

        echo json_encode(['message'=> 'Sales tab data fetched', 'users_count' => $users_count, 'repeated_users_count' => $repeated_users_count, 'top_diets' => $topdiets, 'top_categories' => $topcategories, 'top_products' => $topproducts,'all_categories' => $all_filt_cat_final, 'all_diets' => $all_filt_diet_final, 'total_sale' => $total_sale, 'status' => TRUE]);
        return;
    }
    
    
    public function linecharttab($date_from,$date_to,$cat,$diet){
        $repeated_users = DB::table('order')
                    ->select(DB::raw('count(order.id) as total'))
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('order.users_id')
                    ->get();
        $repeated_users = json_decode(json_encode($repeated_users), true);
        $repeated_users_count = 0;
        $users_count = 0;
        if (!empty($repeated_users)) {
            foreach ($repeated_users as $key => $value) {
                if ($value['total'] > 1) {
                   $repeated_users_count += 1; 
                }
                $users_count += 1;
            }
        }
        $date1 = date_create($date_from);
        $date2 = date_create($date_to);
        $diff = date_diff($date1,$date2);
        $datecount = $diff->format("%a");
        
        $products = DB::table('order')
                    ->select('products.id as id',DB::raw('sum(order_items.grant_total) as value'), 'products.name as label')
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('products', 'products.id', '=', 'order_items.products_id')
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('products.id')
                    ->get();
                    //->paginate(10);
        $all_products = json_decode(json_encode($products), true);
        usort($all_products, function ($a, $b) { return $b['value'] - $a['value']; });
        
        if ($datecount > 31) {
            $categories = DB::table('order')
                        ->select('category.id as id',DB::raw('sum(order_items.grant_total) as value'), 'category.name as label',DB::raw('DATE_FORMAT(order.order_at,"%Y-%M") as date'),DB::raw('DATE_FORMAT(order.order_at,"%Y%m") as sortcol'))
                        ->join('order_items', 'order.id', '=', 'order_items.order_id')
                        ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                        ->join('category', 'category.id', '=', 'product_category.category_id')
                        ->where('order.status', 'DELIVERED')
                        ->whereDate('order.order_at', '>=', $date_from)
                        ->whereDate('order.order_at', '<=', $date_to)
                        ->groupBy('category.id','order.order_at')
                        ->get();
        } else {
            $categories = DB::table('order')
                        ->select('category.id as id',DB::raw('sum(order_items.grant_total) as value'), 'category.name as label',DB::raw('DATE_FORMAT(order.order_at,"%Y-%m-%d") as date'),DB::raw('DATE_FORMAT(order.order_at,"%Y%m%d") as sortcol'))
                        ->join('order_items', 'order.id', '=', 'order_items.order_id')
                        ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                        ->join('category', 'category.id', '=', 'product_category.category_id')
                        ->where('order.status', 'DELIVERED')
                        ->whereDate('order.order_at', '>=', $date_from)
                        ->whereDate('order.order_at', '<=', $date_to)
                        ->groupBy('category.id','order.order_at')
                        ->get();            
        }
                    //->paginate(10);
        $all_categories = json_decode(json_encode($categories), true);
 
        usort($all_categories, function ($a, $b) { return $b['value'] - $a['value']; });
        
        $topcategories = array_slice($all_categories,0,5);
        
        
        // Applying categories filter
        $cats = json_decode($cat);
        $all_cat = $all_categories; 
        if (!empty($all_categories) && !empty($cats)) {
            $all_cat = array();
            foreach ($all_categories as $key => $value) {
                if (in_array($value['id'], $cats)) {
                  $all_cat[] = $value;  
                }
            }
        }
        
        $all_filt_cat_final = $all_cat;
       
      
        // Applying diets filter
        $diets = json_decode($diet);
        
        $flag = false;
        if (!empty($cats) || !empty($diets)) {
            if (!empty($cats) && !empty($diets)) { 
             if ($datecount > 31) {   
                $categories_filtered = DB::table('order')
                            ->select('category.id as id','order_items.order_id as oid','order_items.products_id as pid','product_preferenceoptions.preferenceoptions_id as prid','order_items.grant_total as value', 'category.name as label', DB::raw('DATE_FORMAT(order.order_at,"%Y-%M") as date'), DB::raw('DATE_FORMAT(order.order_at,"%Y%m") as sortcol'))
                            ->join('order_items', 'order.id', '=', 'order_items.order_id')
                            ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                            ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                            ->join('category', 'category.id', '=', 'product_category.category_id')
                            ->whereIn('product_category.category_id', $cats)
                            ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                            ->where('order.status', 'DELIVERED')
                            ->whereDate('order.order_at', '>=', $date_from)
                            ->whereDate('order.order_at', '<=', $date_to)
                            ->get();
             } else {
                $categories_filtered = DB::table('order')
                            ->select('category.id as id','order_items.order_id as oid','order_items.products_id as pid','product_preferenceoptions.preferenceoptions_id as prid','order_items.grant_total as value', 'category.name as label', DB::raw('DATE_FORMAT(order.order_at,"%Y-%m-%d") as date'), DB::raw('DATE_FORMAT(order.order_at,"%Y%m%d") as sortcol'))
                            ->join('order_items', 'order.id', '=', 'order_items.order_id')
                            ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                            ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                            ->join('category', 'category.id', '=', 'product_category.category_id')
                            ->whereIn('product_category.category_id', $cats)
                            ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                            ->where('order.status', 'DELIVERED')
                            ->whereDate('order.order_at', '>=', $date_from)
                            ->whereDate('order.order_at', '<=', $date_to)
                            ->get();
             }
             $flag = true;
            } else if (!empty($diets)){
             if ($datecount > 31) {
                $categories_filtered = DB::table('order')
                           ->select('category.id as id','order_items.order_id as oid','order_items.products_id as pid','product_preferenceoptions.preferenceoptions_id as prid','order_items.grant_total as value', 'category.name as label', DB::raw('DATE_FORMAT(order.order_at,"%Y-%M") as date'), DB::raw('DATE_FORMAT(order.order_at,"%Y%m") as sortcol'))
                           ->join('order_items', 'order.id', '=', 'order_items.order_id')
                           ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                           ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                           ->join('category', 'category.id', '=', 'product_category.category_id')
                           ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                           ->where('order.status', 'DELIVERED')
                           ->whereDate('order.order_at', '>=', $date_from)
                           ->whereDate('order.order_at', '<=', $date_to)
                           ->get();
             } else {
                $categories_filtered = DB::table('order')
                           ->select('category.id as id','order_items.order_id as oid','order_items.products_id as pid','product_preferenceoptions.preferenceoptions_id as prid','order_items.grant_total as value', 'category.name as label', DB::raw('DATE_FORMAT(order.order_at,"%Y-%m-%d") as date'), DB::raw('DATE_FORMAT(order.order_at,"%Y%m%d") as sortcol'))
                           ->join('order_items', 'order.id', '=', 'order_items.order_id')
                           ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                           ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                           ->join('category', 'category.id', '=', 'product_category.category_id')
                           ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                           ->where('order.status', 'DELIVERED')
                           ->whereDate('order.order_at', '>=', $date_from)
                           ->whereDate('order.order_at', '<=', $date_to)
                           ->get();
             }
              $flag = true;
            }

            if ($flag) {
                $all_cat = json_decode(json_encode($categories_filtered), true);
                $all_filt_cat_final = array();
                if (!empty($all_cat)) {
                    $pre_all_cat = array();
                    foreach ($all_cat as $key => $value) {
                        if (!isset($check[$value['id']."_".$value['oid']."_".$value['pid']])) {
                            $check[$value['id']."_".$value['oid']."_".$value['pid']] = true;
                            $pre_all_cat[] = array("id" => $value['id'], "value" => $value['value'], "label" => $value['label'], "date" => $value['date'], "sortcol" => $value['sortcol']); 
                        }
                    }
                    $all_filt_cat = array();
                    $sum = array();
                    foreach ($pre_all_cat as $key => $value) {
                        if (!isset($sum[$value["id"]])) {
                            $sum[$value["id"]] = 0;
                        }
                        $sum[$value["id"]] += $value["value"];
                        $sum[$value["id"]] = $sum[$value["id"]].".00";
                        $all_filt_cat[$value["id"]] = array("id" => $value['id'], "value" => $sum[$value["id"]], "label" => $value['label'], "date" => $value['date'], "sortcol" => $value['sortcol']);
                    }
                    foreach ($all_filt_cat as $key => $value) {
                        $all_filt_cat_final[] = $value;
                    }
                } else {
                    $all_filt_cat_final = array();
                }
            }
        }
        
      
        $linegraphdata = array();
        $linegraphdata_final = array();
        $yaxis = array();
        $total_sale = 0;
        if(!empty($all_filt_cat_final)) {
            foreach ($all_filt_cat_final as $key => $value) {
                $linegraphdata[$value['date']]['day'] = $value['date'];
                $linegraphdata[$value['date']]['sortcol'] = $value['sortcol'];
                if (!isset($linegraphdata[$value['date']][$value['label']])) {
                    $linegraphdata[$value['date']][$value['label']] = 0;
                }
                if (!isset($linegraphdata[$value['date']]['value'])) {
                    $linegraphdata[$value['date']]['value'] = 0;
                }
                $linegraphdata[$value['date']][$value['label']] += $value['value'];
                $linegraphdata[$value['date']]['value'] += $value['value'];
                array_push($yaxis,$value['label']);
            }
            foreach ($linegraphdata as $key => $value) {
                $total_sale += $value['value']; 
                unset($value['value']);
                $linegraphdata_final[] = $value;
            }
            $yaxis = array_unique($yaxis);
        }
        
        $y_axis = array();
        if (!empty($yaxis)) {
            foreach ($yaxis as $key => $value) {
                array_push($y_axis,$value);
            }
        }
        
        $linegraph_data = array();
        if (!empty($linegraphdata_final)) {
            foreach ($linegraphdata_final as $key => $value) {
                foreach ($yaxis as $ykey => $yvalue) {
                    if (!isset($value[$yvalue])) {
                        $value[$yvalue] = 0;
                    } else {
                        $temp = $value[$yvalue];
                        unset($value[$yvalue]);
                        $value[$yvalue] = $temp;
                    }
                }
                // array_push($linegraph_data,$value);
                $linegraph_data[$value['day']] = $value;
            }
        }

        $lgraph_data = array();
        if ($datecount > 31) {
            $temp_date_from = date("Y-F", strtotime($date_from));
            $temp_date_to = date("Y-F", strtotime($date_to));
            $month_array = array($temp_date_from);
            while($temp_date_from != $temp_date_to){
                $temp_date_key = date("Ym", strtotime($temp_date_from. ' +1 months'));
                $temp_date_from = date("Y-F", strtotime($temp_date_from. ' +1 months'));
                $month_array[$temp_date_key] = $temp_date_from; 
            }
            foreach ($month_array as $key => $value) {
                if (isset($linegraph_data[$value])) {
                    array_push($lgraph_data ,$linegraph_data[$value]);
                } else {
                    $temp_array = array();
                    $temp_array['day'] = $value;
                    $sortcol = $key;
                    $temp_array['sortcol'] = $sortcol;
                    foreach ($y_axis as $ykey => $yvalue) {
                      $temp_array[$yvalue] = 0;
                    }
                    array_push($lgraph_data ,$temp_array);                        
                }
            }

            usort($lgraph_data, function ($a, $b) { return $a['sortcol'] - $b['sortcol']; });
            foreach ($lgraph_data as $key => $value) {
                unset($lgraph_data[$key]['sortcol']);
            }
        } else {
            if (!empty($linegraph_data)) {
                $temp_date_to = date("Y-m-d", strtotime($date_to. ' +1 days'));
                $period = new DatePeriod(
                     new DateTime($date_from),
                     new DateInterval('P1D'),
                     new DateTime($temp_date_to)
                );
                foreach ($period as $key => $value) {
                    $nowdate = $value->format('Y-m-d');
                    $sortcol = $value->format('Ymd');
                    if (isset($linegraph_data[$nowdate])) {
                        array_push($lgraph_data ,$linegraph_data[$nowdate]);
                    } else {
                        $temp_array = array();
                        $temp_array['day'] = $nowdate;
                        $temp_array['sortcol'] = $sortcol;
                        foreach ($y_axis as $ykey => $yvalue) {
                          $temp_array[$yvalue] = 0;
                        }
                        array_push($lgraph_data ,$temp_array);                        
                    }
                }
                
                usort($lgraph_data, function ($a, $b) { return $a['sortcol'] - $b['sortcol']; });
                foreach ($lgraph_data as $key => $value) {
                    unset($lgraph_data[$key]['sortcol']);
                }
            }
        }
        
        $total_sale = number_format($total_sale,2);
        
        // Note the following
        // $topproducts, // For the top 5 products
        // $all_cat, // For all the categories, this we can use in pie graph, categories filter applied

        echo json_encode(['message'=> 'Sales line chart tab data fetched','users_count' => $users_count, 'repeated_users_count' => $repeated_users_count,'linegraph_ykeys' => $y_axis,'linegraph_data' => $lgraph_data, 'total_sale' => $total_sale, 'status' => TRUE]);
        return;
    }
  
    
    public function destinationtab($date_from,$date_to,$cat,$diet){
        $repeated_users = DB::table('order')
                    ->select(DB::raw('count(order.id) as total'))
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('order.users_id')
                    ->get();
        $repeated_users = json_decode(json_encode($repeated_users), true);
        $repeated_users_count = 0;
        $users_count = 0;
        if (!empty($repeated_users)) {
            foreach ($repeated_users as $key => $value) {
                if ($value['total'] > 1) {
                   $repeated_users_count += 1; 
                }
                $users_count += 1;
            }
        }
        $date1 = date_create($date_from);
        $date2 = date_create($date_to);
        $diff = date_diff($date1,$date2);
        $datecount = $diff->format("%a");
        
        $products = DB::table('order')
                    ->select('products.id as id',DB::raw('sum(order_items.grant_total) as value'), 'products.name as label')
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('products', 'products.id', '=', 'order_items.products_id')
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('products.id')
                    ->get();
                    //->paginate(10);
        $all_products = json_decode(json_encode($products), true);
        usort($all_products, function ($a, $b) { return $b['value'] - $a['value']; });
        
        $categories = DB::table('order')
                    ->select('category.id as id',DB::raw('sum(order_items.grant_total) as value'), 'category.name as label','order.flight_number as flight_number')
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                    ->join('category', 'category.id', '=', 'product_category.category_id')
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('category.id','order.flight_number')
                    ->get();            
                    //->paginate(10);
        $all_categories = json_decode(json_encode($categories), true);
 
        usort($all_categories, function ($a, $b) { return $b['value'] - $a['value']; });
        
        $topcategories = array_slice($all_categories,0,5);
        
        
        // Applying categories filter
        $cats = json_decode($cat);
        $all_cat = $all_categories; 
        if (!empty($all_categories) && !empty($cats)) {
            $all_cat = array();
            foreach ($all_categories as $key => $value) {
                if (in_array($value['id'], $cats)) {
                  $all_cat[] = $value;  
                }
            }
        }
        
        $all_filt_cat_final = $all_cat;
       
      
        // Applying diets filter
        $diets = json_decode($diet);
        
        $flag = false;
        if (!empty($cats) || !empty($diets)) {
            if (!empty($cats) && !empty($diets)) { 
            $categories_filtered = DB::table('order')
                        ->select('category.id as id','order_items.order_id as oid','order_items.products_id as pid','product_preferenceoptions.preferenceoptions_id as prid','order_items.grant_total as value', 'category.name as label','order.flight_number as flight_number')
                        ->join('order_items', 'order.id', '=', 'order_items.order_id')
                        ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                        ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                        ->join('category', 'category.id', '=', 'product_category.category_id')
                        ->whereIn('product_category.category_id', $cats)
                        ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                        ->where('order.status', 'DELIVERED')
                        ->whereDate('order.order_at', '>=', $date_from)
                        ->whereDate('order.order_at', '<=', $date_to)
                        ->get();
             $flag = true;
            } else if (!empty($diets)){
            $categories_filtered = DB::table('order')
                       ->select('category.id as id','order_items.order_id as oid','order_items.products_id as pid','product_preferenceoptions.preferenceoptions_id as prid','order_items.grant_total as value', 'category.name as label','order.flight_number as flight_number')
                       ->join('order_items', 'order.id', '=', 'order_items.order_id')
                       ->join('product_category', 'order_items.products_id', '=', 'product_category.products_id')
                       ->join('product_preferenceoptions', 'order_items.products_id', '=', 'product_preferenceoptions.products_id')
                       ->join('category', 'category.id', '=', 'product_category.category_id')
                       ->whereIn('product_preferenceoptions.preferenceoptions_id', $diets)
                       ->where('order.status', 'DELIVERED')
                       ->whereDate('order.order_at', '>=', $date_from)
                       ->whereDate('order.order_at', '<=', $date_to)
                       ->get();
              $flag = true;
            }

            if ($flag) {
                $all_cat = json_decode(json_encode($categories_filtered), true);
                $all_filt_cat_final = array();
                if (!empty($all_cat)) {
                    $pre_all_cat = array();
                    foreach ($all_cat as $key => $value) {
                        if (!isset($check[$value['id']."_".$value['oid']."_".$value['pid']])) {
                            $check[$value['id']."_".$value['oid']."_".$value['pid']] = true;
                            $pre_all_cat[] = array("id" => $value['id'], "value" => $value['value'], "label" => $value['label'], "flight_number" => $value['flight_number']); 
                        }
                    }
                    $all_filt_cat = array();
                    $sum = array();
                    foreach ($pre_all_cat as $key => $value) {
                        if (!isset($sum[$value["id"]])) {
                            $sum[$value["id"]] = 0;
                        }
                        $sum[$value["id"]] += $value["value"];
                        $sum[$value["id"]] = $sum[$value["id"]].".00";
                        $all_filt_cat[$value["id"]] = array("id" => $value['id'], "value" => $sum[$value["id"]], "label" => $value['label'], "flight_number" => $value['flight_number']);
                    }
                    foreach ($all_filt_cat as $key => $value) {
                        $all_filt_cat_final[] = $value;
                    }
                } else {
                    $all_filt_cat_final = array();
                }
            }
        }
        
        $linegraphdata = array();
        $linegraphdata_final = array();
        $total_sale = 0;
        if(!empty($all_filt_cat_final)) {
            foreach ($all_filt_cat_final as $key => $value) {
                $linegraphdata[$value['flight_number']]['y'] = $value['flight_number']; 
                if (!isset($linegraphdata[$value['flight_number']]['a'])) {
                    $linegraphdata[$value['flight_number']]['a'] = 0;
                }
                $linegraphdata[$value['flight_number']]['a'] += $value['value'];
            }
            foreach ($linegraphdata as $key => $value) {
                $linegraphdata_final[] = $value;
                $total_sale += $value['a'];
            }
        }
        $total_sale = number_format($total_sale,2);
        
        // Note the following
        // $topproducts, // For the top 5 products
        // $all_cat, // For all the categories, this we can use in pie graph, categories filter applied

        echo json_encode(['message'=> 'Destination tab data fetched','users_count' => $users_count, 'repeated_users_count' => $repeated_users_count,'bargraph_data' => $linegraphdata_final, 'total_sale' => $total_sale, 'status' => TRUE]);
        return;
    }
    
    
    
    public function hourlytab($date_from,$date_to){
        $repeated_users = DB::table('order')
                    ->select(DB::raw('count(order.id) as total'))
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('order.users_id')
                    ->get();
        $repeated_users = json_decode(json_encode($repeated_users), true);
        $repeated_users_count = 0;
        $users_count = 0;
        if (!empty($repeated_users)) {
            foreach ($repeated_users as $key => $value) {
                if ($value['total'] > 1) {
                   $repeated_users_count += 1; 
                }
                $users_count += 1;
            }
        }
        
        $products = DB::table('order')
                    ->select(DB::raw('sum(order_items.grant_total) as sales'), DB::raw('DATE_FORMAT(order.order_at,"%l %p") as time'), DB::raw('DATE_FORMAT(order.order_at,"%H") as date'))
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->where('order.status', 'DELIVERED')
                    ->whereDate('order.order_at', '>=', $date_from)
                    ->whereDate('order.order_at', '<=', $date_to)
                    ->groupBy('date','time')
                    ->get();
                    //->paginate(10);
        $all_products = json_decode(json_encode($products), true);
        $hourly_data = array();
        $total_sale = 0;
        if (!empty($all_products)) {
            foreach ($all_products as $key => $value) {
                $times = explode(' ', $value['time']);
                $right_time = $times[0] + 1;
                if ($right_time == 13) {
                    if ($times[1] == 'AM') {
                        $right_time = '1 AM';
                    } else {
                        $right_time = '1 PM';
                    }
                } elseif ($right_time == 12) {
                    if ($times[1] == 'AM') {
                        $right_time = '12 PM';
                    } else {
                        $right_time = '12 AM';
                    }
                } else {
                    $right_time = $right_time .''.$times[1];
                }
                $newtime = str_replace(' ', '', $value['time']) . ' - ' . str_replace(' ', '', $right_time);
                $value['time'] = $newtime;
                unset($value['date']);
                //array_push($hourly_data,$value);
                $hourly_data[$newtime] = $value;
                $total_sale += $value['sales'];
            }
        }
        
        $timespan_array = array('12AM - 1AM',
                                '1AM - 2AM',
                                '2AM - 3AM',
                                '3AM - 4AM',
                                '4AM - 5AM',
                                '5AM - 6AM',
                                '6AM - 7AM',
                                '7AM - 8AM',
                                '8AM - 9AM',
                                '9AM - 10AM',
                                '10AM - 11AM',
                                '11AM - 12PM',
                                '12PM - 1PM',
                                '1PM - 2PM',
                                '2PM - 3PM',
                                '3PM - 4PM',
                                '4PM - 5PM',
                                '5PM - 6PM',
                                '6PM - 7PM',
                                '7PM - 8PM',
                                '8PM - 9PM',
                                '9PM - 10PM',
                                '10PM - 11PM',
                                '11PM - 12AM');
        $hourly_data_final = array(); 
        foreach ($timespan_array as $key => $value) {
            if (isset($hourly_data[$value])) {
               array_push($hourly_data_final,$hourly_data[$value]); 
            } else {
               array_push($hourly_data_final,array('sales' => 0,'time' => $value));                
            }
        }
        $total_sale = number_format($total_sale,2);
        
        // Note the following
        // $topproducts, // For the top 5 products
        // $all_cat, // For all the categories, this we can use in pie graph, categories filter applied

        echo json_encode(['message'=> 'Hourly tab data fetched','users_count' => $users_count, 'repeated_users_count' => $repeated_users_count, 'hourly_data' => $hourly_data_final, 'total_sale' => $total_sale, 'status' => TRUE]);
        return;
    }
}
