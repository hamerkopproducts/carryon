<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Allergies;
use Validator;
use Illuminate\Support\Facades\Auth;

class AllergiesController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $permissions_data = user_permissions(Auth::user());
          return view('admin.Allergies.Allergies', array('allergies' => $this->fetchAllergies(),
                    'permissions' => $permissions_data['permissions'],
                    'user_has_roles' => $permissions_data['roles']));
    }


    // Getting Allergies list
    private function fetchAllergies(){
        $allergies = DB::table('allergies')
                        ->select('id','name','description','visibility','created_at','updated_at','deleted_at')
                        ->orderBy('created_at', 'desc')
                        ->get();
         return $allergies;
    }


    public function create(Request $request)
    {  
       return view('admin.Allergies.AllergiesCreate');
    }

    public function add(Request $request)
    {
                     
            $validatedData = $request->validate([
                'name' => 'required',
                'description' => 'required',
                'visibility' => 'required',
                'image' => 'required',
                 ]);
            $allerId = '';    
            $timestamp = date('Y-m-d H:i:s');
            $allergies = new Allergies;
            $allergies->name = $request->name;
            $allergies->description = $request->description;
            $allergies->visibility = $request->visibility;
            $allergies->image_path = ''; //$request->image;
            $allergies->created_at = $timestamp;
            $allergies->updated_at = $timestamp;
            $save = $allergies->save();
            $allerId = $allergies->id;
            $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/frontend/allergy'), $input['image']);

            DB::table('allergies')
                ->where('id', $allerId)
                ->update([
                    'image_path' => 'images/frontend/allergy/' . $input['image']
                ]);
            if ($save) $request->session()->flash('status', 'Allergies created successfully');
            return redirect('/admin/allergies');
        }

        public function edit($id)
        {
            //echo  $id;
            $allergies = DB::table('allergies')
                    ->select('id', 'name', 'description', 'visibility', 'image_path' )
                    ->where([
                        ['id', '=', $id],
                    ])
                    ->get();
            return view('admin.Allergies.AllergiesEdit', array('allergies' => $allergies));
        }

        public function update(Request $request){
            
            $id = $request->aller_id;
                if($request->image) {
                $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/frontend/allergy'), $input['image']);
                DB::table('allergies')
                ->where('id', $id)
                ->update([
                    'image_path' => 'images/frontend/allergy/' . $input['image']
                ]);
            
            }
          $allergie=  DB::table('allergies')
                ->where('id', $id)
                ->update([
                    'name'       => $request->post('name'),
                    'description'=> $request->post('description'),
                    'visibility' => $request->post('visibility'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                if ( $allergie) $request->session()->flash('status', 'Allergies updated successfully');
                return redirect('/admin/allergies');
        }


        public function delete ($id)
        {
            //echo $id;
            DB::table('allergies')->where('id', $id)->delete();
            return redirect('/admin/allergies');
        }
            
    } 
   

