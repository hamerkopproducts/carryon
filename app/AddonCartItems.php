<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddonCartItems extends Model
{
    protected $table = 'addons_cart_items';

}
