<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
  
    
    /**
 * The attributes that are mass assignable.
 *
 * @var array
 */
protected $fillable = [
    'name', 'description',  'visibility','position','image_path','alternative_image_path'
];
}