<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'notifications';

    protected $fillable = [
        'users_id','message','created_at' , 'updated_at'
    ];
}