<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Customer  extends Authenticatable
{
    protected $table = 'Users';
  
    
    /**
 * The attributes that are mass assignable.
 *
 * @var array
 */
protected $fillable = [
    'name', 'email',  'phone', 'nationality','type'
];
}
