<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Cartitems extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'cart_items';

    protected $fillable = [
        'cart_id', 'products_id','item_count', 'grant_total','sub_total','tax_total','created_at' , 'updated_at'
    ];
}