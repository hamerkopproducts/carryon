<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddonsOrderItems extends Model
{
    protected $table = 'addons_order_items';
}
