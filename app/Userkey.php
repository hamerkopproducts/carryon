<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Userkey extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'userkey';

    protected $fillable = [
        'users_id','key','created_at' , 'updated_at'
    ];
}