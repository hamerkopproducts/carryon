<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {        
        return $this->view('emails.test')
            ->from($this->data['from_email'], $this->data['from_name'])
            //->cc($address, $name)
            //->bcc($address, $name)
            ->replyTo($this->data['from_email'], $this->data['from_name'])
            ->subject($this->data['subject'])
            ->with([
                'body_text1' => $this->data['message1'], 
                'body_text2' => $this->data['message2'],
                'user' => $this->data['user'],
                'rand' => $this->data['rand']
            ]);
    }
}