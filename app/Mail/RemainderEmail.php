<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RemainderEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {        
        return $this->view('emails.remaindermail')
                    ->from('noreply@carryon.com', 'CARRYON')
                    ->subject($this->data['subject'])
                    ->with(['cust_name' => $this->data['cust_name'], 'nearest_gate' => $this->data['nearest_gate'], 'order_id' => $this->data['order_id'], 'pickup_point' => $this->data['pickup_point'], 'maplink' => $this->data['maplink']]);
    }
}