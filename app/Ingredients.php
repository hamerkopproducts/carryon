<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Ingredients extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'incredience';

    protected $fillable = [
        'name', 'description', 'visibility' , 'image_path' , 'is_filter'
    ];
}