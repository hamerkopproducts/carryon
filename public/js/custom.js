   $('#banner .slides').cycle({
     slideExpr: '.slide',
     fx: 'fade',
     timeout: 5000,
     auto: true,
     speed: 1000,
     next: "#banner .next-btn",
     fit: 1,
     pager: '#banner .controls .container',
     slideResize: 0,
     containerResize: 0,
     height: 'auto',
     before: onBefore,
     after: onAfter,
     width: null
   });

   $('.slideshow2 .slides2').cycle({
     slideExpr: '.slide',
     fx: 'fade',
     timeout: 5000,
     auto: true,
     speed: 1000,
     next: ".slideshow2 .slide-controls .next",
     prev: ".slideshow2 .slide-controls .prev",
     fit: 1,
     pager: '.slideshow2 .controls2',
     slideResize: 0,
     containerResize: 0,
     height: 'auto',
     before: onBefore,
     after: onAfter,
     width: null
   });