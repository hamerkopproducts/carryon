$(document).ready(function () {
  // Start PieChart
  function drawPieChart(pieobject) {
    var donut = Morris.Donut({
      element: pieobject.pie_element,
      data: pieobject.data,
    });
    donut.options.data.forEach(function (label, i) {
      var legendItem = $('<span></span>').text(label['label'] + " ( " + label['value'] + " )").prepend('<br><span>&nbsp;</span>');
      legendItem.find('span')
        .css('backgroundColor', donut.options.colors[i])
        .css('width', '20px')
        .css('display', 'inline-block')
        .css('margin', '5px');
      $(pieobject.pie_legend_element).append(legendItem)
    });

  }
  // End Piechart


  // Start Sales line graph
  var saleslinegraph = new Morris.Line({
    // ID of the element in which to draw the chart.
    element: 'saleslinegraph',
    // Chart data records -- each entry in this array corresponds to a point on
    // the chart.
    data: [{
        day: '2020-02-20',
        category1: 20,
        category2: 10,
        category3: 15
      },
      {
        day: '2020-02-21',
        category1: 10,
        category2: 13,
        category3: 14
      },
      {
        day: '2020-02-22',
        category1: 30,
        category2: 13,
        category3: 13
      },
      {
        day: '2020-02-23',
        category1: 30,
        category2: 10,
        category3: 15
      },
      {
        day: '2020-02-24',
        category1: 20,
        category2: 10,
        category3: 15
      }
    ],
    resize: true,
    // The name of the data record attribute that contains x-values.
    xkey: 'day',
    // A list of names of data record attributes that contain y-values.
    ykeys: ['category1', 'category2', 'category3'],
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['Category1', 'Category2', 'Category3'],
    lineColors: ['#373651', '#E65A26', '#E65A27']
  });
  // End Sales line graph

  // Start Destination bar graph
  var destinationgraph = Morris.Bar({
    element: 'destinationgraph',
    resize: true,
    data: [{
        y: 'EK112',
        a: 100
      },
      {
        y: 'EK113',
        a: 75
      },
      {
        y: 'EK114',
        a: 50
      }
    ],
    lineColors: ['#0b62a4'],
    pointFillColors: ['#00ff00'],
    pointStrokeColors: ['#00ff00'],
    xkey: 'y',
    ykeys: ['a'],
    labels: ['Sales']
  });
  // End Destination bar graph

  // Start Hourly sales line graph
  var hourlysaleslinegraph = new Morris.Line({
    // ID of the element in which to draw the chart.
    element: 'hourlysaleslinegraph',
    // Chart data records -- each entry in this array corresponds to a point on
    // the chart.
    data: [{
        time: '12 noon',
        sales: 4
      },
      {
        time: '01 PM',
        sales: 10
      },
      {
        time: '02 PM',
        sales: 14
      },
      {
        time: '03 PM',
        sales: 10
      },
      {
        time: '04 PM',
        sales: 20
      },
      {
        time: '05 PM',
        sales: 20
      },
      {
        time: '06 PM',
        sales: 20
      },
      {
        time: '07 PM',
        sales: 30
      },
      {
        time: '08 PM',
        sales: 40
      },
      {
        time: '09 PM',
        sales: 10
      },
      {
        time: '10 PM',
        sales: 30
      },
      {
        time: '11 PM',
        sales: 2
      },
    ],
    resize: true,
    // The name of the data record attribute that contains x-values.
    xkey: 'time',
    // A list of names of data record attributes that contain y-values.
    ykeys: ['sales'],
    parseTime: false,
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['Sales'],
    lineColors: ['#373651']
  });
  // End Hourly sales line graph

  // Bootstrap tab
  $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
    let target = $(e.target).attr("href") // activated tab
    // start - to remove the selected bug in bootstrap tab
    $("#reports .nav-link").removeClass("active");
    $(this).addClass("active");
    // end - to remove the selected bug in bootstrap tab
    switch (target) {
      case "#sales":
        getSalesData({
          url: `${base_url}/admin/reports/salestab/${$('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD')}/[]/[]`,
          category: '[]',
          diet: '[]',
          start_date: $('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD'),
          end_date: $('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD')
        }).
        then((data) => {
            promiseThenSales(data);
          }).then((data) => {
            saleslinegraph.redraw();
            $(window).trigger('resize');
          })
          .catch((data) => {
            console.error(data);
          });
        break;
      case "#saleslinechart":
        saleslinegraph.redraw();
        $(window).trigger('resize');
        break;
      case "#destination":
        destinationgraph.redraw();
        $(window).trigger('resize');
        break;
      case "#hourlysales":
        hourlysaleslinegraph.redraw();
        $(window).trigger('resize');
        break;
    }
  });
  // End Bootstrap tab

  function spinner(id) {
    // Start Spinner
    var opts = {
      lines: 13, // The number of lines to draw
      length: 38, // The length of each line
      width: 17, // The line thickness
      radius: 45, // The radius of the inner circle
      scale: 1, // Scales overall size of the spinner
      corners: 1, // Corner roundness (0..1)
      color: '#119125', // CSS color or array of colors
      fadeColor: 'transparent', // CSS color or array of colors
      speed: 1, // Rounds per second
      rotate: 0, // The rotation offset
      animation: 'spinner-line-shrink', // The CSS animation name for the lines
      direction: 1, // 1: clockwise, -1: counterclockwise
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      className: 'spinner', // The CSS class to assign to the spinner
      top: '25%', // Top position relative to parent
      left: '50%', // Left position relative to parent
      shadow: '0 0 1px transparent', // Box-shadow for the lines
      position: 'absolute' // Element positioning
    };

    var target = document.getElementById(id);
    var spinner = new Spin.Spinner(opts).spin(target);

    return $(".spinner");
    // End Spinner
  }


  // Start DateRange picker
  var start = moment().subtract(29, 'days');
  var end = moment();

  function callback(start, end) {
    $('#daterange-sales span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  }

  $('#daterange-sales').daterangepicker({
    startDate: start,
    endDate: end,
    maxDate: moment(),
    dateLimit: {
      days: 90
    },
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  }, callback);
  callback(start, end);
  // End Daterange picker

  // Start Select 2 boxes - category and diet
  $(".select-2-filter_category").select2({
    placeholder: "All"
  });
  $(".select-2-filter_diet").select2({
    placeholder: "All"
  });
  // End Select 2 boxes - category and diet

  function getSalesData(params) {
    spinner('sales');
    return new Promise((resolve, reject) => {
      $.ajax({
        url: params.url,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
          resolve(data);
        },
        error: function (error) {
          reject(error);
        }
      });
    });
  }

  getSalesData({
    url: `${base_url}/admin/reports/salestab/${$('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD')}/[]/[]`,
    category: '[]',
    diet: '[]',
    start_date: $('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD'),
    end_date: $('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD')
  }).then((data) => {
    promiseThenSales(data);
  }).catch((data) => {
    spinner('sales').remove();
  });

  function promiseThenSales(data) {
    let top5_diet_html = Object.keys(data.top_diets).length === 0 ? '</br> No Data Available' : '';
    let top5_category_html = Object.keys(data.top_categories).length === 0 ? '</br> No Data Available' : '';
    let top5_products_html = Object.keys(data.top_products).length === 0 ? '</br> No Data Available' : '';
    let total_sales = data.total_sale === 0 ? '</br> No Data' : 'AED ' + data.total_sale;
    let users_count = data.users_count === 0 ? '</br> No Data' : data.users_count;
    let repeated_users_count = data.repeated_users_count === 0 ? '</br> No Data' : data.repeated_users_count;

    // start - clearing the pie chart and its legends for regeneration
    $("#category-pie").empty();
    $("#diet-pie").empty();
    $("#category-pie-legend").html('');
    $("#diet-pie-legend").html('');
    // end - clearing the pie chart and its legends for regeneration

    // start - total sales
    total_sales = `<p class="card-text text-center"> ${total_sales}</p>`;
    $("#total_sales").html(total_sales);
    // end - total sales

    // start - users count
    users_count = `<p class="card-text text-center"> ${users_count}</p>`;
    $("#users_count").html(users_count);
    // end - users count

    // start - repeated users count
    repeated_users_count = `<p class="card-text text-center"> ${repeated_users_count}</p>`;
    $("#repeated_users_count").html(repeated_users_count);
    // end - repeated users count

    // start - populating html for top 5 category
    data.top_categories.forEach((item, index) => {
      top5_category_html += `<li class="list-group-item">${item.label} - AED ${item.value}</li>`
    });
    $("#top-categories").html(top5_category_html);
    // end - populating html for top 5 category


    // start - populating html for top 5 diets
    data.top_diets.forEach((item, index) => {
      top5_diet_html += `<li class="list-group-item">${item.label} - AED ${item.value}</li>`
    });
    $("#top-diets").html(top5_diet_html);
    // end - populating html for top 5 diets

    // start - populating html for top 5 products
    data.top_products.forEach((item, index) => {
      top5_products_html += `<li class="list-group-item">${item.label} - AED ${item.value}</li>`
    });
    $("#top-products").html(top5_products_html);
    // end - populating html for top 5 products

    // start - creating pie chart
    Object.keys(data.all_categories).length === 0 ? $("#category-pie").html("No Data Available") : drawPieChart({data :data.all_categories, pie_element:'category-pie', pie_legend_element:'#category-pie-legend'});
    Object.keys(data.all_diets).length === 0 ? $("#diet-pie").html("No Data Available") : drawPieChart({data: data.all_diets, pie_element: 'diet-pie', pie_legend_element: '#diet-pie-legend'});
    // end - creating pie chart

    // start - removing spinner object from DOM
    spinner('sales').remove();
    // end - removing spinner object from DOM
  }

  $("#filter").click(function () {
    let category = $("#filter_category").val() === null ? '[]' : `[${$("#filter_category").val()}]`;
    let diet = $("#filter_diet").val() === null ? '[]' : `[${$("#filter_diet").val()}]`;
    getSalesData({
      url: `${base_url}/admin/reports/salestab/${$('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD')}/${category}/${diet}`,
      category: category,
      diet: diet,
      start_date: $('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD'),
      end_date: $('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD')
    }).then((data) => {
      promiseThenSales(data);
    }).catch((data) => {
      spinner('sales').remove();
    });
  });

});