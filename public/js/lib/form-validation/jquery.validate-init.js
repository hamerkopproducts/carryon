
var form_validation = function() {
    var e = function() {
            jQuery(".form-valide").validate({
                ignore: [],
                errorClass: "invalid-feedback animated fadeInDown",
                errorElement: "div",
                errorPlacement: function(e, a) {
                    jQuery(a).parents(".form-group > div").append(e)
                },
                highlight: function(e) {
                    jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
                },
                success: function(e) {
                    jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
                },
                rules: {
                    "role": {
                        required: !0,
                        minlength: 3
                    },
                    "description": {
                        required: !0,
                    },

                    "name": {
                        required: !0,
                    },
                    "email": {
                        required: !0,
                        email:!0,
                    },
                    "role_id": {
                        required: !0,
                    },
                    "password": {
                        required: !0,
                        minlength: 6
                    },

                    "product_name": {
                        required: !0,
                    },
                    "product_sku": {
                        required: !0,
                    },
                    "product_actual_price": {
                        required: !0,
                    },
                    "product_discount_price": {
                        required: !0,
                    },
                    // "product_allergies[]": {
                    //     required: !0,
                    // },
                    "product_categories[]": {
                        required: !0,
                    },
                    "product_incredients[]": {
                        required: !0,
                    },
                    
                },
                messages: {
                    "rolename": {
                        required: "Please enter a role",
                    },
                    "description": "Please enter a description",
                }
            })
        }
    return {
        init: function() {
            e(), jQuery(".js-select2").on("change", function() {
                jQuery(this).valid()
            })
        }
    }
}();
jQuery(function() {
    form_validation.init()
});