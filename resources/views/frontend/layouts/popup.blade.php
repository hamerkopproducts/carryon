@php
   $flight_popup_top = DB::select('select html_content from cms where visibility = "YES" and short_code = "flight_popup_top" and deleted_at IS NULL');
   $flight_popup_bottom = DB::select('select html_content from cms where visibility = "YES" and short_code = "flight_popup_bottom" and deleted_at IS NULL');
@endphp
<div class="order" id="flight_popup" style="display:none !important;">
    <button style="display:none;" class="trigger-order">trigger</button>
    <button class="close"><i class="fa fa-times" aria-hidden="true"></i> </button>
    {!! $flight_popup_top[0]->html_content !!}
<!--    <h1>Fresh, healthy meals for your flight</h1>-->
    <div class="form">
        <span class="flight ui-widget">
        <input type="password" style="display:none"/><!-- to override prompt -->
        <input id='flight' type="text" placeholder="Enter your flight number" class="flight"/>
      </span>
        <input type="password" style="display:none"/><!-- to override prompt -->
        <span class="date"><input id="flight_date_dt" type="text" placeholder="Select your flight date" class="flight_date" readonly onfocus="this.removeAttribute('readonly');" /></span>
        
        <span id="flight_data" style="font-size: 16px;border-radius: 7px;color: #4a8046;"></span>
        <!-- <span class="flight-info ">No flight added yet.</span> -->

        <span id="timecheck_error" style="color:red;font-weight: bold;font-size: 18px"></span>
        <span class="submit" ><input id='fligh_submit' type="button" value="Let's Go" /></span>
    </div>
    <p>{!! $flight_popup_bottom[0]->html_content !!} 
<!--        <a href="{{asset('terms-and-conditions')}}" target="_blank">Terms & Conditions</a> and <a href="{{asset('privacy-policy')}}" target="_blank">Privacy Policy.</a>-->
    </p>
</div>

<script type="text/Javascript">
$(document).ready(function(){

  var can_order = localStorage.getItem("can_order");
  var flight_date = localStorage.getItem("flight_date");
  var flight = localStorage.getItem("flight");
  if( can_order && $.inArray(flight, ['N/A', 'null', null, '']) == -1) {

    var flight_updated_at = localStorage.getItem("flight_updated_at");
    if(flight_updated_at != null && new Date() - new Date(flight_updated_at) > 3600000) {
      localStorage.setItem("flight", 'N/A');
      localStorage.setItem("flight_date", 'N/A');
      localStorage.setItem("can_order", 0);
      localStorage.setItem("cart_key", "");
    } else {
      // Update to current time
      localStorage.setItem("flight_updated_at", new Date());
    }

      $('input#flight').val( flight );
      $(".flight-info").html("Selected flight - "+ flight);
      $('input.flight_date').val( flight_date );
  }


$('.submit').click(function(){
    var flight = $("#flight").val();
    flight = flight.toUpperCase();
    var flight_date = $(".flight_date").val();
    var flight_date_original = flight_date;
    if(flight == ''){
        $("#flight").addClass('textbox error').focus();
        return false;
    }
    if(flight_date == ''){
        $(".flight_date").addClass('textbox error');
        return false;
    }
    $("#flight").removeClass('textbox error');
    $(".flight_date").removeClass('textbox error');

    $('#overlay').show();
    var res = flight_date.split("-");
    flight_date = res[1]+'/'+res[0]+'/'+res[2]; 
    $.ajax( {
      url: "{{url('/flight/show')}}",
      dataType: "json",
      data: {
        fl_no: flight,
        date: flight_date,
        fl_rnd: Math.random()
      },
      success: function( data ) {
        if(data.status == true) {
          localStorage.setItem("can_order", 1);
          $("#timecheck_error").text("");
          localStorage.setItem("flight",flight);
          localStorage.setItem("flight_date", flight_date_original);
          localStorage.setItem("flight_updated_at", new Date());

          $('.flight_number_sidebar').html('<span>Flight number:</span>'+ flight );
          $('.flight_deliver_date_sidebar').html('<span>Delivery:</span>' + flight_date );
          alert('Flight data updated');
          $('#flight_popup button.close').trigger('click');
          $("body").removeClass("popup-open");
          var url_part = location.href.split('/')[location.href.split('/').length - 1].trim();
          if( $.inArray(url_part, ['terms-and-conditions', 'menu'] ) == -1 && location.href.indexOf('productdetail') == -1 ) {
            location.href = "<?php echo route('our_menu') ?>";
          }
        } else {
          localStorage.setItem("can_order", 0);
          localStorage.setItem("flight", 'N/A');
          localStorage.setItem("flight_date", 'N/A');
          if(data.message == 'time elapsed') {
            $("#timecheck_error").html(`We can only accept orders 6 hours before departure time. So sorry!`);
          } else {
            $("#timecheck_error").html(data.message);
          }
          $(".flight-info").html("No flight added.");
          $('#flight_data').hide();
        }
      },
      complete: function() {
        $('#overlay').hide();
      }
    });
});    
   
//$('#flight_popup').hide();
//Order button in mobile view click
  $('.trigger-order').click(function(){
      $('#flight_popup').show();
  });
       
//Close pop on clicking close icon in pop
$(".close").click(function(){
   $('#flight_popup').hide();
});
    
$(".food").click(function(){
    $('#flight_popup').show();
})

});

</script>
