<style>
 input[type=search] {
            background-color: #f1f1f1;
            border-color: #2e2e2e;
            border-style: solid;
            border-width: 2px 2px 2px 2px;
            outline: none;
            padding: 10px 20px 10px 20px;
            width: 250px;
            }
            ul.ui-autocomplete {
            color: #000000 !important;
            -moz-border-radius: 15px;
            border-radius: 1px;
            max-height: 180px; overflow-y: auto;
            overflow-x: hidden; 
            z-index: 99999!important; 
            font-size: 82% !important;
            font-weight: normal !important;
            }
            #errcode,#errphone
            {
            color: white;
            /* font-size: 8; */
            text-align: center;
            }
</style>
@php
  $login_bg = DB::select('select image_path from cms_slider where visibility = "YES" and short_code = "login_bg" and deleted_at IS NULL');
  $register_bg = DB::select('select image_path from cms_slider where visibility = "YES" and short_code = "sign_up_bg" and deleted_at IS NULL');

   $footer_address = DB::select('select page_title, html_content from cms where visibility = "YES" and short_code = "footer_address" and deleted_at IS NULL');
   $footer_email = DB::select('select page_title, html_content from cms where visibility = "YES" and short_code = "footer_email" and deleted_at IS NULL');
   $footer_phone = DB::select('select page_title, html_content from cms where visibility = "YES" and short_code = "footer_phone" and deleted_at IS NULL');
   $footer_copyright = DB::select('select page_title, html_content from cms where visibility = "YES" and short_code = "footer_copyright" and deleted_at IS NULL');
   $footer_content = DB::select('select page_title, html_content from cms where visibility = "YES" and short_code = "footer_content" and deleted_at IS NULL');
   $footer_socialmedias = DB::select('select social_media_link, social_media_name from cms where visibility = "YES" and short_code = "socialmedia" and deleted_at IS NULL ORDER BY position ASC');
@endphp
<footer id="footer">
      <div class="top-footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-2 col-md-5 column order-md-2">
              <div class="content">
                <div class="logo">
                  <a href="#">
                    <img src="{{asset('images/frontend/logo2.svg')}}" alt="logo">
                  </a>
                </div>
<!--                <p class="d-none">We understand human bodies go through changes when flying and the high altitude changes not only the
                  palette, but also the needs, therefore we ensure that the taste and nutritional values sit right.</p>-->
                                <p class="d-none">{!! $footer_content[0]->html_content !!}</p>
              </div>
            </div>
            <div class="col-lg-10 col-md-7">
              <div class="row">
                <div class="col-lg-3 col-md-6 column">
<!--                  <h6>Phone</h6>
                  <a href="#" class="phone">+971 50 298 7966</a>-->

                </div>
                <div class="col-lg-3 col-md-6 column">
                  <h6>Contact Information</h6>
<!--                  <a href="#" class="mail">hello@carryondxb.com</a><br>
                  <a href="#" class="phone">+971 50 298 7966</a>-->
                  <a href="#" class="mail">{!! $footer_email[0]->html_content !!}</a><br>
                  <a href="#" class="phone">{!! $footer_phone[0]->html_content !!}</a>
                </div>

                <div class="col-lg-3 col-md-6 column">
                  <h6>Quick Links</h6>
                  <ul class="footer-links">
                    <li><a href="{{asset('about-us')}}">About Us</a></li>
                    <li><a href="{{route('our_menu')}}">Menu</a></li>
                    <li><a href="{{asset('faq')}}">FAQs</a></li>
<!--                    <li><a href="#">Blog</a></li>-->
                  </ul>
                </div>
                <div class="col-lg-3 col-md-6 column d-none">
                  <h6>Address</h6>
                  <address>
<!--                    <p>
                      HWH General Trading LLC<br>
                      451 Lounge <br>
                      Airport Terminal 3 <br>
                      Dubai, UAE
                    </p>-->
                      {!! $footer_address[0]->html_content !!}
                  </address>
                </div>
                <div class="col-lg-3 col-md-6 column">
                  <h6>follow us</h6>
                  <ul class="sociables">
                      @php
                      foreach($footer_socialmedias as $footer_socialmedia){
                      if($footer_socialmedia->social_media_link !=''){
                      @endphp
                        <li>
                          <a href="{{$footer_socialmedia->social_media_link}}"><i class="fa fa-{{$footer_socialmedia->social_media_name}}" aria-hidden="true"></i></a>
                        </li>
                      @php
                      }
                      }
                      @endphp
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="bottom-footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6">
              <p><a href="{{asset('terms-and-conditions')}}" target="_blank" href="#">Terms and Conditions</a>, <a href="{{asset('privacy-policy')}}" target="_blank" href="#">Privacy Policy</a>, <a href="{{asset('terms-of-use')}}" target="_blank" href="#">Terms of Use</a></p>
            </div>
            <div class="col-lg-4 col-md-6">
              <ul class="payment-gateways">
                <li>
                  <img src="{{asset('images/visa.png')}}" alt="img">
                </li>
                <li>
                  <img src="{{asset('images/master-card.png')}}" alt="img">
                </li>
                <li>
                  <img src="{{asset('images/verified-by-visa.png')}}" alt="img">
                </li>
                <li>
                  <img src="{{asset('images/master-card-secure.png')}}" alt="img">
                </li>
               
              </ul>
            </div>
            <div class="col-lg-5 text-right">
<!--              <p class="copyright">Copyright © 2020 HWH General Trading LLC. All rights reserved.</p>-->
                <p class="copyright">{!! $footer_copyright[0]->html_content !!}</p>
              <a href="#" class="scroll-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <div class="modal" id="forgot_password_top_menu" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="checkout" style="background:url('{{ url($login_bg[0]->image_path) }}')">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
            </button>
            <div class="content">
              <div id="login-form" class="active">
                <p id="forgot_validation_messages_top_menu"></p>
                <h1 id="forgot_success_message_top_menu" style="color: white!important;"></h1>
                <div class="form">
                  <ul>
                    <li>
                      <input id="forgot_email_top_menu" name="email" type="text" class="textbox" placeholder="Enter your Email">
                    </li>
                    <li>
                      <input type="submit" value="Send Password" class="btn button1" id="forgot_password_submit_top_menu">
<label for="terms"><a style="color:white !important;font-size: 19px" href="#"data-toggle="modal" data-target="#login" id="login_link" >
<span style="text-decoration: underline; text-decoration-color: white;font-size: 25 px !important;padding-left:20px"><b> Back to Login</b></a></span></label>                   
                    </li>
                  </ul>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal" id="login" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="checkout" id="checkout_image" style="background:url('{{ url($login_bg[0]->image_path) }}')">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
            </button>
            <div class="content">
              <div id="login-form" class="active">
                <p id="login_validation_messages_top_menu"></p>
                <h1 id="login_success_message_top_menu" style="color: white!important;"></h1>
                <div class="form">
                  <ul>
                    <li>
                      <input id="login_email_top_menu" name="login_email" type="text" class="textbox" placeholder="Email">
                    </li>
                    <li>
                      <input id="login_password_top_menu" name="login_password" type="Password" class="textbox" placeholder="Password">
                    </li>
                    <li>
                        <a href="#"data-toggle="modal" data-target="#forgot_password_top_menu" id="forgot_password_link">Forgot password?</a>
                      <span style="margin-left: 50px;"><a href="#"data-toggle="modal" style="font-size: 17px;" data-target="#register" id="register_link">Don't have an account ? <span style="text-decoration: underline; text-decoration-color: white;font-size: 25 px !important;"><b> Sign Up</b></span></a></span>
                    </li>
                    <li>
                      <input type="submit" value="Login" class="btn button1" id="login_submit_top_menu">
                    </li>
                  </ul>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="register" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="checkout" style="background:url('{{ url($register_bg[0]->image_path) }}')">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
            </button>
            <div class="content">
              <div id="register-form" class="active">
                  
                  <p id="validation_messages_top_menu"></p>
                  <h1 id="success_message_top_menu" style="color: white!important;"></h1>
                <div class="form">
                  <ul>
                    <li class="half">
                        <input id="first_name_top_menu" name="first_name" type="text" class="textbox" placeholder="Enter your first name">
                    </li>
                    <li class="half">
                      <input id="last_name_top_menu" name="last_name"  type="text" class="textbox" placeholder="Enter your last name">
                    </li>
                    <li class="half">
                      <input  id="email_top_menu" name="email" type="text" class="textbox" placeholder="Enter your email address">
                    </li>
                    <li class="half">
                      <!-- <input id="phone" name="phone" type="text" class="textbox" placeholder="Enter your phone"  >
                      <span id="errphone"></span> -->
                      <input type="text" name="phone" id="phone_top_menu"  class="textbox">
                      <span id="errphone"></span>
                    </li>
                   
                    <li class="half">
                            <select id="country_top_menu" name="country" class="textbox search_input" placeholder="Select Country">
                            </select>
<!--                     <input id="country_top_menu" type="text" name="country" class="textbox search_input" placeholder="Enter country of residence">-->

<label for="terms"><a style="color:white !important;font-size: 19px" href="#"data-toggle="modal" data-target="#login" id="login_link" >
<br>
Already have an account ?<span style="text-decoration: underline; text-decoration-color: white;font-size: 25 px !important;"><b> Login</b></a></span></label>
                    </li>

                     
                    

                   
                    <!-- <a href="#"data-toggle="modal" data-target="#login" id="login_link">Already have an account ? Login</a> -->
                   
                       

                    <li class="half">
                      <input name="terms" type="checkbox" id="terms_top_menu" /> <label for="terms"><a style="color:white !important;" href="{{asset('terms-and-conditions')}}" target="_blank">* I read and accepted the terms and
                       conditions.</a></label>
                      <input type="submit" value="Register" class="btn button1" id="register_submit_top_menu">
                    </li>
                  </ul>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="overlay">
        <div class="lds-dual-ring"></div>
    </div>

<script type="text/Javascript">
$(document).ready(function(){

    $("#forgot_password_link,#forgot_password_link_checkout,#login_link,#register_link,#login_link_checkout,#register_link_checkout").click(function(){
        $('.fa-times').trigger('click');
    });
    
  $("#phone_top_menu,#code_top_menu").keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         $("#errcode,#errphone").html("Numbers Only").show().fadeOut("slow");
         return false;
                  }
          });
       
    // var input = document.querySelector("#code");
    // window.intlTelInput(input, {
    // utilsScript:"./js/phone_plugin/utils.js"
    // });

    var input = document.querySelector("#phone_top_menu");
    window.intlTelInput(input, {
    preferredCountries: ["ae"],
    initialCountry:"ae",
    utilsScript: "{{asset('js/phone_plugin/utils.js')}}",
    });
    $('.iti__country').click(function(){
      $('#phone_top_menu').val($(this).children("span:nth-child(3)").html()+'-');
    })
 

    function phoneNumValidation(phone) {
      reg = /\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)-\d{5,10}$/
      return reg.test(phone);
    }

//$('.order').hide();
$('#register_submit_top_menu').click(function(){
    $("#validation_messages_top_menu").text('');
    var first_name = $("#first_name_top_menu").val();
    var last_name = $("#last_name_top_menu").val();
    var email = $("#email_top_menu").val();
    var code = $("#code_top_menu").val();
    var phone = $("#phone_top_menu").val();
    var country = $("#country_top_menu").val();
    var terms = $("#terms_top_menu").val();
    var commonmessage = 'Provide data to all the feilds';
    if(first_name == ''){
        $("#validation_messages_top_menu").text(commonmessage);
        $("#first_name_top_menu").addClass('textbox error').focus();
        return false;
    } else {
        $("#first_name_top_menu").removeClass('error');
    }
    if(last_name == ''){
        $("#validation_messages_top_menu").text(commonmessage);
        $("#last_name_top_menu").addClass('textbox error').focus();
        return false;
    } else {
        $("#last_name_top_menu").removeClass('error');
    }
    if(email == ''){
        $("#validation_messages_top_menu").text(commonmessage);
        $("#email_top_menu").addClass('textbox error').focus();
        return false;
    } else {
        $("#email").removeClass('error');
    }
    if(!validateEmail(email)){
        $("#email_top_menu").addClass('textbox error').focus();
        $("#validation_messages_top_menu").text('Enter valid Email Address');
        return false;
    } else {
        $("#email_top_menu").removeClass('error');
    }
    // if(code == ''){
    //     $("#validation_messages").text(commonmessage);
    //     $("#code").addClass('textbox error').focus();
    //     return false;
    // } else {
    //     $("#code").removeClass('error');
    // }
    if(phone == ''){
        $("#validation_messages_top_menu").text(commonmessage);
        $("#phone_top_menu").addClass('textbox error').focus();
        return false;
    } else {
      if( ! phoneNumValidation(phone) ) {
        $("#validation_messages_top_menu").text('Invalid Mobile Number');
        $("#phone_top_menu").addClass('textbox error').focus();
        return false;
      }
      $("#phone_top_menu").removeClass('error');
    } 
    // if(isNaN(phone)){
    //     $("#validation_messages").text('Phone number should be numerals');
    //     $("#phone").addClass('textbox error').focus();
    //     return false;
    // } else {
    //     $("#phone").removeClass('error');
    // }   
    
    
    if(country == ''){
        $("#validation_messages_top_menu").text(commonmessage);
        $("#country_top_menu").addClass('textbox error').focus();
        return false;
    } else {
        $("#country_top_menu").removeClass('error');
    }
    if($('#terms_top_menu').prop('checked') == false){
        $("#validation_messages_top_menu").text('Please Accept Terms and Conditions');
        return false;
    } else {
        $("#terms_top_menu").removeClass('error');
    }
    $('#overlay').show();
    $.ajax( {
      url: "{{url('/customer/registration')}}",
      dataType: "json",
      data: {
        first_name: first_name,
        last_name: last_name,
        email: email,
        code: code,
        phone: phone,
        country: country,
      },
      success: function( data ) {
          if(data.error == '1'){
              $("#validation_messages_top_menu").text(data.message);
              $('#overlay').hide();
          } 
          if(data.error == '0'){
              $(".form").html("");
              $("#success_message_top_menu").text(data.message);
              //$('.btn-secondary').trigger('click');
              $('#overlay').hide();
                function close_popup(){
                     $('#register').slideUp();
                     location.reload();
                  };
               window.setTimeout( close_popup, 2000 );
          }
      }
    } );
    
    function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test( $email );
    }
});

$('#login_submit_top_menu').click(function(){
    $("#login_validation_messages_top_menu").text('');
    var email = $("#login_email_top_menu").val();
    var password = $("#login_password_top_menu").val();
    var commonmessage = 'Provide data to all the feilds';
    if(email == ''){
        $("#login_validation_messages_top_menu").text(commonmessage);
        $("#login_email_top_menu").addClass('textbox error').focus();
        return false;
    } else {
        $("#login_email_top_menu").removeClass('error');
    }
    if(!validateEmail(email)){
        $("#login_email_top_menu").addClass('textbox error').focus();
        $("#login_validation_messages_top_menu").text('Enter valid Email Address');
        return false;
    } else {
        $("#login_email_top_menu").removeClass('error');
    }    
    if(password == ''){
        $("#login_validation_messages_top_menu").text(commonmessage);
        $("#login_password_top_menu").addClass('textbox error').focus();
        return false;
    } else {
        $("#login_password_top_menu").removeClass('error');
    }
   $('#overlay').show();
    $.ajax( {
      type: "POST",
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: "{{url('/customer/login')}}",
      dataType: "json",
      data: {
        email: email,
        password: password,
      },
      success: function( data ) {
          if(data.error == '1'){
              $("#login_validation_messages_top_menu").text(data.message);
              $('#overlay').hide();
          } 
          if(data.error == '0'){
              
              //$(".form").html("");
              //$("#login_success_message_top_menu").text(data.message);
              $('#overlay').hide();
              //alert(data.message);
              //$('.btn-secondary').trigger('click');
                function close_popup(){
                     $('#login_top_menu').slideUp();
                     location.reload();
                  };
               window.setTimeout( close_popup, 10 );
          }
      }
    } );
    
    function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test( $email );
    }
});

function validateZipCode(elementValue){
  var zipCodePattern = /^\d{6}$/;
  return zipCodePattern.test(elementValue);
}

$('.billing-information #billing_zip').on('focusout', function() {
  var billing_zip = $(this).val();
  if( ! validateZipCode(billing_zip) ) {
    $('#billing-zip-error').show();
    $("#billing_zip").addClass('textbox error').focus();
  } else {
    $('#billing-zip-error').hide();
    $("#billing_zip").removeClass('error');
  }
});

$('#billing_submits').on('click', function() {
  var billing_zip = $('.billing-information #billing_zip').val();
  if( ! validateZipCode(billing_zip) ) {
    $('#billing-zip-error').show();
    $("#billing_zip").addClass('textbox error').focus();
  } else {
    $('#billing-zip-error').hide();
    $("#confirm").modal('show');
  }
});

$('.order-reviewed').click(function(){
    $('#confirm').modal('toggle');
    $("#validation_messages").text('');
    var billing_name = $("#billing_name").val();
    var billing_city = $("#billing_city").val();
    var billing_zip = $("#billing_zip").val();
    var billing_country = $("#billing_country").val();
    var billing_street = $("#billing_street").val();
    var billing_street_number = $("#billing_street_number").val();
    var billing_apartment_number = $("#billing_apartment_number").val();
    var commonmessage = 'Provide data to all the feilds';
    if(billing_name == ''){
        $("#validation_messages").text(commonmessage);
        $("#billing_name").addClass('textbox error').focus();
        return false;
    } else {
        $("#billing_name").removeClass('error');
    }
    if(billing_city == ''){
        $("#validation_messages").text(commonmessage);
        $("#billing_city").addClass('textbox error').focus();
        return false;
    } else {
        $("#billing_city").removeClass('error');
    }
    if(billing_zip == ''){
        $("#validation_messages").text(commonmessage);
        $("#billing_zip").addClass('textbox error').focus();
        return false;
    } else {
        if( ! validateZipCode(billing_zip) ) {
          $('#billing-zip-error').show();
          $("#billing_zip").addClass('textbox error').focus();
          return false;
        } else {
          $('#billing-zip-error').hide();
          $("#billing_zip").removeClass('error');
        }
        
    }
    if(billing_country == ''){
        $("#validation_messages").text(commonmessage);
        $("#billing_country").addClass('textbox error').focus();
        return false;
    } else {
        $("#billing_country").removeClass('error');
    }
    if(billing_street == ''){
        $("#validation_messages").text(commonmessage);
        $("#billing_street").addClass('textbox error').focus();
        return false;
    } else {
        $("#billing_street").removeClass('error');
    } 
    if(billing_street_number == ''){
        $("#validation_messages").text(commonmessage);
        $("#billing_street_number").addClass('textbox error').focus();
        return false;
    } else {
        $("#billing_street_number").removeClass('error');
    } 
    if(billing_apartment_number == ''){
        $("#validation_messages").text(commonmessage);
        $("#billing_apartment_number").addClass('textbox error').focus();
        return false;
    } else {
        $("#billing_apartmnent_number").removeClass('error');
    } 
   
    $.ajax( {
      type: "POST",
      url: "{{url('/home/addbilling')}}",
      dataType: "json",
      data: {
        billing_name: billing_name,
        billing_city: billing_city,
        billing_zip: billing_zip,
        billing_country: billing_country,
        billing_street: billing_street,
        billing_street_number: billing_street_number,
        billing_apartment_number: billing_apartment_number,
      },
      success: function( data ) {
          if(data.error == '1'){
              $("#validation_messages").text(data.message);
          } 
          if(data.error == '0'){
              alert(data.message);
              checkFlightTime();
          }
      }
    } );
    
});

function checkFlightTime() {
  var flight_date = localStorage.getItem("flight_date");
  var res = flight_date.split("-");
  flight_date = res[1]+'/'+res[0]+'/'+res[2];
  var flight = localStorage.getItem("flight");
  $('#overlay').show();
  $.ajax( {
      url: "{{url('/flight/show')}}",
      dataType: "json",
      data: {
          fl_no: flight,
          date: flight_date,
          fl_rnd: Math.random()
      },
      success: function( data ) {
          $('#overlay').hide();
          if(data.status == false) {
              $('.trigger-order').trigger('click');
              alert('Please update flight information');

              return;
          }
          if(data.status == true) {
            location.href = "<?php echo route('payment_page')?>";
          }
      }
  });
}

cart_list_side_view();
        var flight_date = localStorage.getItem("flight_date");
        var flight = localStorage.getItem("flight");
        $('.flight_number_sidebar').html('<span>Flight number:</span>'+ flight );
        $('.flight_deliver_date_sidebar').html('<span>Delivery:</span>' + flight_date );

        function cart_list_side_view() {
            $('#overlay').show();
            $('.pay_proceed').addClass('disabled');
            $.ajax({
                url: "<?php echo route('cart_list')?>",
                method: "post",
                data: {
                    key: localStorage.getItem('cart_key')
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    data = JSON.parse(data);
                    var sideon_cart_string = '';
                    if(data.status == true) {
                        for(var inc=0; inc < data.eachdata.length; inc++) {
                            sideon_cart_string += sideon_cart_list(
                                                data.eachdata[inc].product_id, 
                                                data.eachdata[inc].product_name, 
                                                data.eachdata[inc].item_count_item_total, 
                                                data.eachdata[inc].grant_total_item_total,
                                                data.eachdata[inc].image_path
                                            );
                        }
                        $('.sideon-cart-list').html(sideon_cart_string);
                      if(data.eachdata.length == 0) {
                          var url = $(location).attr('href'),
                          parts = url.split("/"),
                          last_part = parts[parts.length-1]; 
                          if(last_part == "checkout" || last_part == "payment" || last_part == "billingaddress"){
                            setTimeout(() => {
                              location.href = "<?php echo route('our_menu') ?>";
                            }, 2000);
                            // if no items redirect to menu.
                            alert('No items in cart');
                          }
                          $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                          $('.sub_total').text('0.00');
                          $('.tax_total').text('0.00');
                          $('.grand_total').text('AED 0');
                          $('.total_cart_item_count').text(0);
                      } else {
                          $('.pay_proceed').removeClass('disabled');
                          $('.sub_total').text(data.maindata.sub_total_total);
                          $('.tax_total').text(data.maindata.tax_total_total);
                          grant_total_item_total = parseFloat(data.maindata.grant_total_total);
                          $('.grand_total').html('<span>AED</span>' + grant_total_item_total);
                          $('.total_cart_item_count').text(data.maindata.item_count_total);
                      }
                        
                    } else {
                        $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                        if(data.message != 'Key is not passed')
                            alert(data.message);
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

        function sideon_cart_list(product_id, product_name, item_count_item_total, grant_total_item_total, path) {
          grant_total_item_total = parseFloat(grant_total_item_total);
            return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${item_count_item_total}"></li>
            <li><button product_id=${product_id} type="button" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small></h4>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            </tr>`;
        }

        //disabling past date from datepicker
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        //initializing datepicker
        $("#flight_date_dt").removeClass('hasDatepicker');
        $("#flight_date_dt").datepicker({
          minDate: today,
          dateFormat: 'dd-mm-yy',
            onSelect: function(dateText) {
                var res = dateText.split("-");
                dateText = res[1]+'/'+res[0]+'/'+res[2]; 
                checkFlight(dateText);
                $(".flight_date").removeClass('textbox error');
            }
        });

        $('#flight').on('input', function() {
          $(".flight_date").val('');
        });

        function checkFlight(dateText) {
          var flight = $("#flight").val();
          var flight_date = $(".flight_date").val();
          if(flight == ''){
              $("#flight").addClass('textbox error').focus();
              return false;
          }
          if(flight_date == ''){
              $(".flight_date").addClass('textbox error');
              return false;
          }
          $("#flight").removeClass('textbox error');
          $('#overlay').show();
          $("#timecheck_error").text("");
          $.ajax( {
            url: "{{url('/flight/show')}}",
            dataType: "json",
            data: {
              fl_no: flight,
              date: dateText,
              fl_rnd: Math.random()
            },
            success: function( data ) {
              if(data.status == true) {
                $("#timecheck_error").text("");
                $(".flight-info").html("Selected flight - "+ data.flight);
                $('#flight_data').html(`Destination: ${data.destination}<br>
                                        Departure time: ${data.departure}\n`).
                  css('padding', '10px').css('box-shadow', ' 0 0 8px 1px #ddd').show();
              } else {
                if(data.message == 'time elapsed') {
                  $("#timecheck_error").html(`We can only accept orders 6 hours before departure time. So sorry!`);
                } else {
                  $("#timecheck_error").html(data.message);
                }
                $(".flight-info").html("No flight added.");
                $('#flight_data').hide();
              }
            },
            complete: function() {
              $('#overlay').hide();
            }
          });
        }
        // populate country name in country feild //
         var countryNames = 
          ["United Arab Emirates","Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];    
             var country_top_menu = $("#country_top_menu");
            $(countryNames).each(function (index, element) {
                var option = $("<option />");
 
                //Set Customer Name in Text part.
                option.html(element);
 
                //Set Customer CustomerId in Value part.
                option.val(element);
 
                //Add the Option element to DropDownList.
                country_top_menu.append(option);
            });
       // populate country name in country feild  end //

});    


$('#forgot_password_submit_top_menu').click(function(){
    $("#forgot_validation_messages_top_menu").text('');
    var email = $("#forgot_email_top_menu").val();
    var commonmessage = 'Provide data to all the feilds';
    if(email == ''){
        $("#forgot_validation_messages_top_menu").text('Enter your email address');
        $("#forgot_email_top_menu").addClass('textbox error').focus();
        return false;
    } else {
        $("#forgot_email_top_menu").removeClass('error');
    }
    if(!validateEmail(email)){
        $("#forgot_email_top_menu").addClass('textbox error').focus();
        $("#forgot_validation_messages_top_menu").text('Enter valid Email Address');
        return false;
    } else {
        $("#forgot_email_top_menu").removeClass('error');
    }    
    
   $('#overlay').show();
    $.ajax( {
      type: "POST",
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: "{{url('/customer/forgotpassword')}}",
      dataType: "json",
      data: {
        email: email,
      },
      success: function( data ) {
          if(data.error == '1'){
              $("#forgot_validation_messages_top_menu").text(data.message);
              $('#overlay').hide();
          } 
          if(data.error == '0'){
              
              $(".form").html("");
              $("#forgot_success_message_top_menu").text(data.message);
              $('#overlay').hide();
              //alert(data.message);
              //$('.btn-secondary').trigger('click');
                function close_popup(){
                     $('#forgot_email_top_menu').slideUp();
                     location.reload();
                  };
               window.setTimeout( close_popup, 2000 );
          }
      }
    } );
        function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test( $email );
    }
     } );
// $('.submit').click(function(){
//     var flight = $("#flight").val();
//     var flight_date = $(".flight_date").val();
//     if(flight == ''){
//         $("#flight").addClass('textbox error').focus();
//         return false;
//     }
//     if(flight_date == ''){
//         $(".flight_date").addClass('textbox error');
//         return false;
//     }
    
//     $.ajax( {
//       url: "{{url('/flight/timecheck')}}",
//       dataType: "json",
//       data: {
//         flight: flight,
//         flight_date: flight_date,
//       },
//       success: function( data ) {
          
//           localStorage.setItem("can_order", data.can_order);
//           if(data.can_order == '0'){
//               localStorage.setItem("flight", 'N/A');
//               localStorage.setItem("flight_date", 'N/A');
//               $("#timecheck_error").text("You can only place order 4 hours before you selected flight departure time");
//           } 
//           if(data.can_order == '1'){
//             localStorage.setItem("flight",flight);
//             localStorage.setItem("flight_date", flight_date);
//             localStorage.setItem("flight_updated_at", new Date());

//             $('.flight_number_sidebar').html('<span>Flight number:</span>'+ flight );
//             $('.flight_deliver_date_sidebar').html('<span>Delivery:</span>' + flight_date );

//             var url_part = location.href.split('/')[location.href.split('/').length - 1].trim();
//             if( $.inArray(url_part, ['terms-and-conditions', 'menu'] ) == -1 && location.href.indexOf('productdetail') == -1 ) {
//               location.href = "<?php echo route('our_menu') ?>";
//             }
//           }
//         }
//     }); 
// });

//hide duration//
// <br>Duration: ${data.duration} Hrs
//    $(function () {
//        var availableTags = 
//          ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];
//         $(".search_input").autocomplete({
//            source: availableTags
//        });
//    });
    
    </script>












