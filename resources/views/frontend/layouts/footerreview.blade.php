    <!-- /footer -->
    <!-- Javascript -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<!--    <script src="{{asset('js/vendor/jquery-ui-1.9.2.custom.min.js')}}"></script>-->
    <script src="{{asset('js/vendor/jquery.matchHeight-min.js')}}"></script>
    <script src="{{asset('js/vendor/popper.min.js')}}"></script>
    <script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/vendor/ofi.browser.js')}}"></script>
    <script src="{{asset('js/vendor/jquery.cycle.all.min.js')}}"></script>
    <script src="{{asset('js/vendor/headroom.min.js')}}"></script>
    <script src="{{asset('js/vendor/jQuery.headroom.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    
    <script src="{{asset('js/vendor/aos-min.js')}}"></script>
    <!-- Custom JS Code for all pages -->
    <script src="{{asset('js/phone_plugin/intlTelInput.js')}}"></script>
    <script src="{{ asset('js/lib/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/lib/jquery-ui/jquery.multi-select.min.js') }}"></script>
    <script src="{{ asset('js/lib/jquery-ui/rating.js') }}"></script>

    @include('frontend.home.home_script')