<!DOCTYPE html>
<html>
 
 @include('frontend.layouts.header')
 @include('frontend.layouts.footer')

<!--  <style>
      .ui-autocomplete { z-index:2147483647!important; }
      .shadow {
  -moz-box-shadow:    2px 2px 2px 1px #ccc;
  -webkit-box-shadow: 2px 2px 2px 1px #ccc;
  box-shadow:         2px 2px 2px 1px #ccc;
}
  </style>-->
    <body>
        <!--[if lt IE 10]>
              <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
        <![endif]-->
        <div id="container">
            <!-- header -->
            @include('frontend.layouts.headermenu')
            <!-- /header -->
            <!-- </script> -->
            <!-- Order -->
            @include('frontend.layouts.popup')
            <!-- Order -->
                        <script>
                $(document).ready(function() {
                  //$('.pay_proceed').hide();
                  $('.order').hide();

                });
                </script>
             <!-- content -->
            @yield('content')
             <!-- content -->

            <!-- footer -->
            @include('frontend.layouts.footermenu')

            <button style="display:none;" class="btn button1 btn-block view-basket" data-toggle="modal" data-target="#basket">view basket</button>
            @include('frontend.layouts.basket_popup')
            
            <!-- /footer -->
            </div>
    </body>

</html>