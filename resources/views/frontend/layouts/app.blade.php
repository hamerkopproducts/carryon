<!DOCTYPE html>
<html>

    @include('frontend.layouts.header')
    @include('frontend.layouts.footer')

  <style>
      .ui-autocomplete { z-index:2147483647!important; }
/*      .shadow {
  -moz-box-shadow:    2px 2px 2px 1px #ccc;
  -webkit-box-shadow: 2px 2px 2px 1px #ccc;
  box-shadow:         2px 2px 2px 1px #ccc;
}*/
  </style>
<body>
  <!--[if lt IE 10]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
  <![endif]-->
  <div id="container">
    <!-- header -->
    @include('frontend.layouts.headermenu')
    <!-- /header -->
    <div class="fixed-footer">
      <a class="btn button1 trigger-order">Order now</a>
    </div>
    <!-- Order -->
    @include('frontend.layouts.popup')
    <!-- Order -->
    <!-- banner -->
    <div id="banner">
      <button class="close"><i class="fa fa-times" aria-hidden="true"></i> </button>
      <div class="slides">      
            @if (count($home_sliders) > 0)
            @php
            $count = 1;
            @endphp
            @foreach ($home_sliders as $home_slider)
            @php
            $contains = strpos($home_slider->image_path, 'mp4');
            if($contains){
            @endphp
                <div class="slide video-slide" data-slide="slide{{$count}}">
                  <figure class="figure1">
                    <img class="w-100" src="{{asset($home_slider->cover_image_path)}}" alt="img">
                  </figure>
                  <a href="#" class="play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                  <video id="slide1" muted controls>
                    <source src="{{asset($home_slider->image_path)}}" type="video/mp4">
                  </video>
                  <div class="description">
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-4 col-md-7">
                          <h2 class="display-1">{{$home_slider->description}}</h2>
                          @if($home_slider->description != '' && $home_slider->link_text != '')
                          <a href="{{ asset($home_slider->link) }}" class="btn button1">{{ $home_slider->link_text }}</a>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 
            @php
            } else {
            @endphp
                    <div class="slide" data-slide="slide{{$count}}">
                    <figure class="figure1">
                      <img class="w-100" src="{{asset($home_slider->image_path)}}" alt="img">
                    </figure>
                    <div class="description">
                      <div class="container">
                        <div class="row">
                          <div class="col-lg-4 col-md-7">
                            <h2 class="display-1">{{$home_slider->description}}</h2>
                            @if($home_slider->description != '' && $home_slider->link_text != '')
                            <a href="{{ asset($home_slider->link) }}" class="btn button1">{{ $home_slider->link_text }}</a>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
            @php
            } 
            $count++;
            @endphp
            @endforeach
            @endif
          
          
          

<!--        <div class="slide" data-slide="slide2">
          <figure class="figure1">
            <img class="w-100" src="{{asset('images/frontend/Carry-On_banner_green.png')}}" alt="img">
          </figure>
          <div class="description">
            <div class="container">
              <div class="row">
                <div class="col-lg-4 col-md-7">
                  <h2 class="display-1">Your online food ordering portal</h2>
                  <a href="{{asset('about-us')}}" class="btn button1">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="slide" data-slide="slide3">
          <figure class="figure1">
            <img class="w-100" src="{{asset('images/frontend/banner-img1.jpg')}}" alt="img">
          </figure>
          <div class="description">
            <div class="container">
              <div class="row">
                <div class="col-lg-4 col-md-7">
                  <h2 class="display-1">Restaurant’s ordering portal</h2>
                  <a href="{{asset('about-us')}}" class="btn button1">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="slide video-slide" data-slide="slide4">
          <figure class="figure1">
            <img class="w-100" src="{{asset('images/frontend/banner-img1.jpg')}}" alt="img">
          </figure>
          <a href="#" class="play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
          <video id="slide4" muted controls>
            <source src="{{asset('videos/movie1.mp4')}}" type="video/mp4">
          </video>
          <div class="description">
            <div class="container">
              <div class="row">
                <div class="col-lg-4 col-md-7">
                  <h2 class="display-1">Your own
                    online food ordering</h2>
                  <a href="{{asset('about-us')}}" class="btn button1">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="controls">
        <div class="container"></div>
      </div>
    </div>
    <!-- banner -->
    <div id="content">
      <!-- section -->
      <div class="section">
        @yield('content')
      </div>
      <!-- /section -->
      <!-- section -->
      <div class="section about">
        <div class="fuel-for-journey">
          <img src="{{asset('images/frontend/fuel-for-journey.png')}}" alt="Fuel For Journey" />
        </div>
        <div class="container">
          <header>
                  {!! $home_page_content_title[0]
                  ->html_content !!}
          </header>
        </div>
        <div class="container-fluid">
          <div class="row">
@php
   
   $home_page_middle = DB::select('select title, description, image_path from cms_slider where visibility = "YES" and short_code = "homepage_middle" and deleted_at IS NULL');
@endphp
            <div class="col-md-7 order-md-2">
              <div class="video">
                <a href="#">
                  <img class="w-100" src="{{ asset($home_page_middle[0]->image_path) }}" alt="img">
                  <span class="play-btn">
                    <i class="fa fa-play" aria-hidden="true"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-md-5 d-flex">
              <div class="box1 align-self-center">
                  {!! $home_page_content[0]
                  ->html_content !!}
<!--                <h2>We believe
                  eating right should be easy for
                  everyone.</h2>
                <p>We started CarryOn because we believe eating fresh, nutritious dishes every day should not only be
                  easy for everyone, but it is also important when traveling too.</p>-->
                <footer>
                  <a href="{{asset('about-us')}}" class="btn btn-link">Learn more about CarryOn</a>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /section -->
    </div>

    <button style="display:none;" class="btn button1 btn-block view-basket" data-toggle="modal" data-target="#basket">view basket</button>
    @include('frontend.layouts.basket_popup')



    <!-- footer -->
    @include('frontend.layouts.footermenu')
     
     <!-- footer -->
  </div>
</body>

</html>


























