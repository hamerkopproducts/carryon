<!DOCTYPE html>
<html>
 
 @include('frontend.layouts.header')
 @include('frontend.layouts.footer')

    <body>
        <div id="container">
            <!-- header -->
            @include('frontend.layouts.headermenu')
            @include('frontend.layouts.popup')
            
            @yield('content')
            @include('frontend.layouts.footermenu')

            <button style="display:none;" class="btn button1 btn-block view-basket" data-toggle="modal" data-target="#basket">view basket</button>
            @include('frontend.layouts.basket_popup')
            </div>
    </body>

</html>