@extends('frontend.layouts.appdetail')
@section('content')
            <div id="content">
                <!-- section -->
                <div class="wrapper">
                    <div class="container">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Our Menu </a></li>
                                <li class="breadcrumb-item"><a href="#">{{ $product_category_name }}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    @php
                                    echo $productdetails[0]['product_name'];
                                    @endphp
                                </li>
                            </ol>
                        </nav>
                        <div class="tabs1">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                @if (count($categories) > 0)
                                @php
                                $count = 1;
                                @endphp
                                @foreach ($categories as $category)
                                @php
                                $category_selection_class = ($product_category_id == $category->id)?"active":"";
                                @endphp                           
                                <li class="nav-item">
                                    <a href="{{asset('product/menu#'.$category->id)}}" class="{{ $category_selection_class }}">{{ $category->name }}</a>
                                </li>       
                                @endforeach
                                @else
                                <p>No Data</p>
                                @endif               
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 column dish">
                                <header class="mb-md-0">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <h2>
                                                @php
                                                echo $productdetails[0]['product_name'];
                                                @endphp
                                            </h2>
                                        </div>
                                        <div class="col-md-5 text-md-right">
                                            <a item_count={{$productdetails[0]['item_count']}} 
                                                product_id={{$productdetails[0]['product_id']}} 
                                                href="#" 
                                                class="btn button1 add-item-cart"><i class="fa fa-cart-plus" 
                                                aria-hidden="true"></i> Add for AED
                                                @php
                                                echo decimal_format_custom($productdetails[0]['actual_price']);
                                                @endphp</a>
                                        </div>
                                    </div>
                                </header>
                                <div class="slideshow2">
                                    <div class="slides2">
                                        @if (count($productdetails) > 0)
                                        @php
                                        $count = 1;
                                        @endphp
                                        @foreach ($productdetails as $productdetail)  
                                        @php
                                            if ($productdetail['type'] != 'THUMBNAIL'){
                                            @endphp
                                                <div class="slide" data-slide="slide{{$count}}">
                                                    <figure class="figure1">
                                                        <img class="w-100" src="{{url($productdetail['product_image_path'])}}" alt="img">
                                                    </figure>
                                                </div>
                                                @php } @endphp
                                            @php
                                            $count++;
                                            @endphp
                                        @endforeach  
                                        @endif
                                    </div>
                                    <div class="slide-controls">
                                        <a href="#" class="prev" id="prev"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                                            <a href="#" class="next" id="next"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="controls2">
                                        </div>
                                    </div>
                                    <ul class="flavour-list">
                                        @foreach ($productallergies as $allergies)                       
                                        <li>
                                            <a data-toggle="tooltip" title="" data-original-title="{{ $allergies->description }}">
                                                <img src="{{ asset($allergies->image_path) }}" alt="img">
                                            </a>
                                        </li>  
                                        @endforeach
                                    </ul>
                                    <!-- @php
                                    echo $productdetails[0]['description'];
                                    @endphp -->
                                </div>
                                <div class="col-lg-4 column pt-md-5">
                                    <div class="widgets pt-md-3">
                                        <div class="widget ingredients">
                                            <div class="header">
                                                <h3>Ingredients</h3>
                                                <a href="#" class="toggle-btn"></a>
                                            </div>
                                            <div class="content">
                                                <ul class="ingredients-list">
                                                    @if (count($ingredients) > 0)
                                                    @php
                                                    $count = 1;
                                                    @endphp
                                                    @foreach ($ingredients as $ingredient)
                                                    <li>
                                                        <div class="image">
                                                            <img src="{{asset($ingredient->image_path)}}" alt="img">
                                                        </div>
                                                        <p>{{$ingredient->name}}</p>
                                                    </li>           
                                                    @endforeach
                                                    @else
                                                    <p>No data to show</p>
                                                    @endif         
                                                </ul>
                                            </div>
                                        </div>
                                        <div style="padding-top:15px;" class="detail-product">
                                        @php
                                            echo $productdetails[0]['description'];
                                        @endphp
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /section -->
                    <!-- section -->
                    <div class="section bg-gray-light also-order">
                        <div class="container">
                            <h3>Also order</h3>
                            <ul class="menu-list">
                                        @if (count($relatedproducts) > 0)
                                        @php
                                        $count = 1;
                                        @endphp
                                        @foreach ($relatedproducts as $relatedproduct)
                                <li>
                                    <div class="food">
                                        <div class="image">
                                            <a href="{{asset($relatedproduct['url_key'])}}"><img class="w-100" src="{{asset($relatedproduct['path'])}}" alt="img"></a>
                                        </div>
                                        <div class="info">
                                            <h2><a href="#">{{ $relatedproduct['name'] }}</a></h2>
                                            <footer>
                                                <ul class="flavour-list">
                                                    @if ( isset($relatedproduct['allergies']) && count($relatedproduct['allergies']) > 0)
                                                    @foreach($relatedproduct['allergies'] as $allergies)
                                                        <li>
                                                            <a href="#" data-toggle="tooltip" title="{{ $allergies->description }}"><img src="{{asset($allergies->image_path)}}"></a>
                                                        </li>
                                                    @endforeach
                                                    @else
                                                    <p></p>
                                                    @endif 
                                                </ul>
                                            </footer>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                @else
                                <p>No data to show</p>
                                @endif 
                            </ul>
                        </div>
                    </div>
                    <!-- /section -->
                </div>
                <script>
                    $(document).ready(function() {
                        setTimeout(() => {
                            $('#flight_popup button.close').trigger('click');
                        }, 200);
//                        $.ajaxSetup({
//                            headers: {
//                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                            }
//                        });
                        function flightAdd() {
                            console.log(localStorage.getItem('can_order'))
                            if( localStorage.getItem('can_order') === null || localStorage.getItem('can_order') === 'null' ) {
                                $('#flight_popup').show();
                                $('.trigger-order').trigger('click');
                                alert('Please add flight information');
                                $('#basket button').trigger('click');
                                return false;
                            }

                            if( localStorage.getItem('can_order') === false || localStorage.getItem('can_order') == 'false' ) {
                                $('#flight_popup').show();
                                $('.trigger-order').trigger('click');
                                alert('Please update flight information');
                                $('#basket button.toggle-btn').trigger('click');
                                return false;
                            }

                            return true;
                        }

                        // function addItemToCart(product_id, newQty, callBack) {
                        //     $('#overlay').show();
                        //     $.ajax({
                        //         url: "<?php echo route('cart_add')?>",
                        //         method: "post",
                        //         data: {
                        //             productsid: product_id,
                        //             item_count: newQty,
                        //             key: localStorage.getItem('cart_key')
                        //         },
                        //         success: function(data) {
                        //             data = JSON.parse(data);
                        //             if(data.status == true) {
                        //                 if(typeof data.cartkey != 'undefined') {
                        //                     localStorage.setItem("cart_key", data.cartkey);
                        //                 }
                        //                 callBack();
                        //                 cart_list_side_view();
                        //                 alert('Your cart updated');
                        //                 $('.add-item-cart').attr('item_count', newQty);
                        //                 //cart_list_side_view();
                        //             } else {
                        //                 alert(data.message);
                        //             }
                        //         },
                        //         complete: function() {
                        //             $('#overlay').hide();
                        //         }
                        //     });
                        // }

                        function cart_list_side_view() {
                            $('#overlay').show();
                            $.ajax({
                                url: "<?php echo route('cart_list')?>",
                                method: "post",
                                data: {
                                    key: localStorage.getItem('cart_key')
                                },
                                success: function(data) {
                                    data = JSON.parse(data);
                                    var sideon_cart_string = '';
                                    if(data.status == true) {
                                        for(var inc=0; inc < data.eachdata.length; inc++) {
                                            sideon_cart_string += sideon_cart_list(
                                                data.eachdata[inc].product_id, 
                                                data.eachdata[inc].product_name, 
                                                data.eachdata[inc].item_count_item_total, 
                                                data.eachdata[inc].grant_total_item_total,
                                                data.eachdata[inc].image_path
                                            );
                                        }
                                        $('.sideon-cart-list').html(sideon_cart_string);
                                        if(data.eachdata.length == 0) {
                                            $('.sub_total').text('0.00');
                                            $('.tax_total').text('0.00');
                                            $('.grand_total').text('AED 0');
                                            $('.total_cart_item_count').text(0);
                                            $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                                        } else {
                                            $('.sub_total').text(data.maindata.sub_total_total);
                                            $('.tax_total').text(data.maindata.tax_total_total);
                                            grant_total_item_total = parseFloat(data.maindata.grant_total_total);
                                            $('.grand_total').html('<span>AED</span>'+ grant_total_item_total );
                                            $('.total_cart_item_count').text(data.maindata.item_count_total);
                                        }
                                        
                                    } else {
                                        $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                                        if(data.message != 'Key is not passed')
                                            alert(data.message);
                                    }
                                },
                                complete: function() {
                                    $('#overlay').hide();
                                }
                            });
                        }

                        function sideon_cart_list(product_id, product_name, item_count_item_total, grant_total_item_total, path) {
                            grant_total_item_total = parseFloat(grant_total_item_total);
                            return `<tr>
                                <td class="product-image">
                                    <figure class="image">
                                        <img class="w-100" src="${path}" alt="img">
                                    </figure>
                                    <ul class="quantity">
                                        <li><button product_id=${product_id} type="button" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
                                        <li><input product_id=${product_id} type="number" class="input-control" value="${item_count_item_total}"></li>
                                        <li><button product_id=${product_id} type="button" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
                                    </ul>
                                </td>
                                <td class="product-details">
                                    <h4>${product_name}
                                    <small></small></h4>
                                </td>
                                <td class="product-price">
                                    AED <strong>${grant_total_item_total}</strong>
                                </td>
                            </tr>`;
                        }
                        
                    });
                </script>
@endsection
