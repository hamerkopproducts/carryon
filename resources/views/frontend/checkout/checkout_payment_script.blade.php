<script type="text/javascript">

    $(document).ready(function() {

        
        var r_try_time = 10;
        var pg_interval = setInterval(() => {
            console.log('asd')
            r_try_time -= 1;
            if(r_try_time > 0) {
                $('#retry_pg').text(`Retry in ${r_try_time}  sec...`);
            } else {
                $('#retry_pg').html('<button onclick="location.reload();" class="btn button1">Retry</button>');
                clearintrval();
            }
        }, 1000);

        function clearintrval() {
            window.clearInterval(pg_interval);
        }

        $('html,body').animate({scrollTop: $("#online_payment").offset().top},'slow');
        $('.pay_proceed').hide();
            setTimeout(() => {
                $('#flight_popup button.close').trigger('click');
            }, 200);
        @if(Session::has('message'))
            $('#overlay').show();
            setTimeout(() => {
                $('#overlay').hide();
                alert("{{ Session::get('message') }}");
            }, 2000);
        @endif
        @if(Session::has('code'))
            if("{{ Session::get('code') }}" == 'PAYED_OK') {
                setTimeout(() => {
                    location.href = "<?php echo route('home_page')?>";
                }, 4000);
                localStorage.setItem("flight", 'N/A');
                localStorage.setItem("flight_date", 'N/A');
                localStorage.setItem("can_order", false);
                localStorage.setItem("cart_key", '');
            }
        @endif

        cart_list_side_view();
        var flight_date = localStorage.getItem("flight_date");
        var flight = localStorage.getItem("flight");
        $('.flight_number_sidebar').html('<span>Flight number:</span>'+ flight );
        $('.flight_deliver_date_sidebar').html('<span>Delivery:</span>' + flight_date );

        function cart_list_side_view() {
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('cart_list')?>",
                method: "post",
                data: {
                    key: localStorage.getItem('cart_key')
                },
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                },
                success: function(data) {
                    data = JSON.parse(data);
                    var sideon_cart_string = '';
                    if(data.status == true) {
                        for(var inc=0; inc < data.eachdata.length; inc++) {
                            sideon_cart_string += sideon_cart_list(
                                                data.eachdata[inc].product_id, 
                                                data.eachdata[inc].product_name, 
                                                data.eachdata[inc].item_count_item_total, 
                                                data.eachdata[inc].grant_total_item_total,
                                                data.eachdata[inc].image_path
                                            );
                        }
                        $('.sideon-cart-list').html(sideon_cart_string);
                        if(data.eachdata.length == 0) {
                            $('.sub_total').text('0.00');
                            $('.tax_total').text('0.00');
                            $('.grand_total').text('AED 0');
                            $('.total_cart_item_count').text(0);
                            $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                        } else {
                            $('.sub_total').text(data.maindata.sub_total_total);
                            $('.tax_total').text(data.maindata.tax_total_total);
                            grant_total_item_total = parseFloat(data.maindata.grant_total_total);
                            $('.grand_total').html('<span>AED</span>' + grant_total_item_total);
                            $('.total_cart_item_count').text(data.maindata.item_count_total);
                        }
                        
                    } else {
                        $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                        if(data.message != 'Key is not passed')
                            alert(data.message);
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

        function sideon_cart_list(product_id, product_name, item_count_item_total, grant_total_item_total, path) {
            grant_total_item_total = parseFloat(grant_total_item_total);
            return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${item_count_item_total}"></li>
            <li><button product_id=${product_id} type="button" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            </tr>`;
        }

    });

</script>