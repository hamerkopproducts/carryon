<script type="text/javascript">

    $(document).ready(function() {
        var url = $(location).attr('href'),
        parts = url.split("/"),
        last_part = parts[parts.length-1]; 
        if(last_part == ""){
            $('#menu_link').removeClass('current');
            $('#home_link').addClass('current');
            
        } else {
            $('#home_link').removeClass('current');
            $('#menu_link').addClass('current');
        }
        
            var clickflag = true;
            var datakey;
            setTimeout(function() {
                var categoryfromurl =  window.location.hash.substr(1);
                if(categoryfromurl !="" && clickflag == true){
                    datakey = 'tab_'+categoryfromurl+'-tab';
                    $("#"+datakey).trigger("click");
                    clickflag=false; 
                }
            }, 1000);  
        
        $('#logout').click(function(){
            $.ajax( {
                url: "{{url('/customer/logout')}}",
                dataType: "json",
                data: {
                },
                success: function( data ) {
                        alert(data.message);
                        localStorage.setItem("flight", 'N/A');
                        localStorage.setItem("flight_date", 'N/A');
                        localStorage.setItem("can_order", false);
                        localStorage.setItem("cart_key", '');
                        setTimeout(() => {
                            location.href = "<?php echo route('home_page')?>";
                        }, 200);
                }
            });
        }); 

        $('#overlay').show();
        $.ajax({
            url: "<?php echo route('menu_dishes')?>",
            method: "post",
            data: {
                key: localStorage.getItem('cart_key')
            },
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == true) {
                    var tab_header = '';
                    var cat_tab_dets = [];
                    setIngredientsFilter(data.incredients);
                    for(var inc=0; inc < data.category_order.length; inc++) {
                        catid = data.category_order[inc];
                        tab_header += tabHeader(data.products[catid].category_id, data.products[catid].category_name, inc);
                        cat_tab_dets.push(itemRender(catid, name, data.products[catid].items, inc) );
                    }
                    $('#menu_tab').html(tab_header);
                    $('.place-holder').remove();
                    for(var incr=0; incr < cat_tab_dets.length; incr++) {
                        $('#menu_tabContent').prepend(cat_tab_dets[incr]);
                    }
                } else {
                    alert(data.message);
                }
                $('.place-holder').remove();
                cart_list_side_view();
            },
            complete: function() {
                $('#overlay').hide();
            }
        });

        function tabHeader(cat_id, name, count) {
            var classs = count == 0 ? 'active' : '';
            return `<li class="nav-item">
                    <a class="nav-link ${classs}" id="tab_${cat_id}-tab" data-toggle="tab" href="#tab_${cat_id}" 
                        role="tab" aria-controls="home" aria-selected="true">${name}</a>
                </li>`;
        }

        function setIngredientsFilter(incredients) {
            var string = '';
            for(var inc=0; inc < incredients.length; inc++) {
                string += `<li>
                                <a style="cursor:pointer;" ingredient_id=${incredients[inc].id} class="ingredient_filter">
                                    <span class="icon">
                                        <img src="${incredients[inc].image_path}" alt="icon">
                                    </span>${incredients[inc].name}
                                </a>
                            </li>`;
            }

            $('.filter-list').html(string);
        }

        $(document).on('click', '.ingredient_filter', function() {
            $('#overlay').show();
            var ingredient_id = $(this).attr('ingredient_id');
            
            if( ! $(this).closest('li').hasClass('active') ) {
                var prdt_incredients = '';
                $('.product_list_with_ingredients').each(function(i, v) {
                    prdt_incredients = $(this).attr('ingredients');
                    if( $.inArray(ingredient_id, prdt_incredients.split(',')) == -1 ) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
                $('.filter-list li').removeClass('active');
                $(this).closest('li').addClass('active');
                setTimeout(() => {
                    $('#overlay').hide();
                }, 500);
            } else {
                $('.product_list_with_ingredients').show();
                $(this).closest('li').removeClass('active');
                setTimeout(() => {
                    $('#overlay').hide();
                }, 500);
            }
        });

        function itemRender(cat_id, name, items, inc) {
            var string = '';
            var classs = inc == 0 ? 'active' : '';
            string += `<div class="tab-pane fade show ${classs} col-lg-8 column" id="tab_${cat_id}" role="tabpanel" aria-labelledby="tab_${cat_id}-tab">
              <ul class="menu-list">`;
              var count = 0;
              var active_or_not = '';
              for(var inc=0; inc < items.length; inc++) {
                    count = items[inc].item_count == null ? 0 : items[inc].item_count;
                    active_or_not = count > 0 ? 'active' : '';
                    pdt_price = parseFloat(items[inc].actual_price);
                    string += `<li class="product_list_with_ingredients" ingredients=${items[inc].incredience}>
                        <div class="food">
                            <div class="image">
                            <a href="${items[inc].url_key}"><img style="height: 280px;" class="w-100" src="${items[inc].thumnail}" alt="img"></a>
                            </div>
                            <div class="info">
                            <ul class="quantity ${active_or_not} product_id_${items[inc].product_id}">
                                <li><button product_id=${items[inc].product_id} type="button" class="less"></button></li>
                                <li><input  product_id=${items[inc].product_id} type="number" class="input-control" value="${count}"></li>
                                <li><button product_id=${items[inc].product_id} type="button" class="add"></button></li>
                            </ul>
                            <p class="price"><span>AED</span> ${pdt_price}</p>
                            <h2><a href="#">${items[inc].product_name}</a></h2>
                            <footer>
                                <ul class="flavour-list">`;
                            for(var allr_inc=0; allr_inc < items[inc].allergies.length; allr_inc++) {
                                string += `<li>
                                    <a data-toggle="tooltip" title="${items[inc].allergies[allr_inc].name}"><img src="${items[inc].allergies[allr_inc].image_path}"
                                        alt="img">
                                    </a>
                                </li>`;
                            }
                    string += `</ul>
                            </footer>
                            </div>
                        </div>
                    </li>`;
                }
            string += `</ul>
            </div>`;

            return string;
        }

        function cart_list_side_view() {
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('cart_list')?>",
                method: "post",
                data: {
                    key: localStorage.getItem('cart_key')
                },
                success: function(data) {
                    data = JSON.parse(data);
                    var sideon_cart_string = '';
                    if(data.status == true) {
                        for(var inc=0; inc < data.eachdata.length; inc++) {
                            sideon_cart_string += sideon_cart_list(
                                                data.eachdata[inc].product_id, 
                                                data.eachdata[inc].product_name, 
                                                data.eachdata[inc].item_count_item_total, 
                                                data.eachdata[inc].grant_total_item_total,
                                                data.eachdata[inc].image_path
                                            );
                        }
                        $('.sideon-cart-list').html(sideon_cart_string);
                        if(data.eachdata.length == 0) {
                            $('.sub_total').text('0.00');
                            $('.tax_total').text('0.00');
                            $('.grand_total').text('AED 0');
                            $('.total_cart_item_count').text(0);
                            $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                        } else {
                            $('.sub_total').text(data.maindata.sub_total_total);
                            $('.tax_total').text(data.maindata.tax_total_total);
                            grant_total_item_total = parseFloat(data.maindata.grant_total_total);
                            $('.grand_total').html('<span>AED</span>' + grant_total_item_total);
                            $('.total_cart_item_count').text(data.maindata.item_count_total);
                        }
                        
                    } else {
                        $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                        if(data.message != 'Key is not passed')
                            alert(data.message);
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

        function sideon_cart_list(product_id, product_name, item_count_item_total, grant_total_item_total, path) {
            grant_total_item_total = parseFloat(grant_total_item_total);
            return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${item_count_item_total}"></li>
            <li><button product_id=${product_id} type="button" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            </tr>`;
        }

        $('#flight_popup').hide();
        if(localStorage.getItem("can_order") == true) {
            $('#flight_popup button.close').trigger('click');
            setTimeout(() => {
                $('#flight_popup button.close').trigger('click');
            }, 200);
        }

    });

</script>