@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <h2>Our Menu</h2>
    <p>Makes your online order best and easy</p>
    @if (count($categories) > 1)
    <div class="tabs1">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            @foreach ($categories as $key => $category)
            @if ($key == 0)
            <li class="nav-item">
              <a class="category active" id="tab" data-key="{{$key+1}}" data-category="{{$category->id}}" data-toggle="tab" href="#tab{{$key+1}}" role="tab"
                    aria-controls="tab{{$key+1}}" aria-selected="true">{{$category->name}}</a>
            </li>
            @else
            <li class="nav-item">
                <a class="category" id="tab{{$key+2}}-tab" data-key="{{$key+1}}" data-category="{{$category->id}}" data-toggle="tab" href="#tab{{$key+1}}" role="tab"
                    aria-controls="tab{{$key+1}}" aria-selected="false">{{$category->name}}</a>
            </li>
            @endif
            @endforeach

        </ul>
        <div class="tab-content" id="myTabContent">
            
        </div>
    </div>
    @endif


</div>

@endsection

<!-- @push('scripts')
<script type="text/javascript">
alert(1)
  $(document).ready(function(){
    @if(count($categories) > 0)
    alert(1)
    getProducts('{{$categories[0]->id}}', 1);
    @endif
    

    $(".category").click(function(){
      getProducts($(this).attr("data-category"), $(this).attr("data-key"));
    });

    function getProducts(category_id, key){
      $.ajax({
        url: "{{url('/product/show')}}",
        dataType: "JSON",
        data: {
            category: category_id
          },
        success: function(data){
          var html = `<div class="tab-pane fade show active" id="tab`+key+`" role="tabpanel" aria-labelledby="tab`+key+`-tab">
                       <ul class="menu-list">`;
          $.each(data, function(){
            var allergy_html = '';
            allergy_html +=  `<footer>
                                    <ul class="flavour-list">`;
            $.each(this.allergies, function(){
              allergy_html += `<li>
                                            <a href="#" data-toggle="tooltip" title="`+this.description+`"><img
                                                    src="{{asset('`+this.image_path+`')}}" alt="img"></a>
                                        </li>`;
            });
            allergy_html += ` </ul>
                                </footer>`;
            html +=
            `
                    <li>
                        <div class="food" data-aos="fade-up">
                            <div class="image">
                                <a href="{{asset('productdetail/`+this.url_key+`')}}"><img class="w-100" src="{{asset('`+this.product_image_path+`')}}"
                                        alt="img"></a>
                            </div>
                            <div class="info">
                                <h2><a href="{{asset('productdetail/`+this.url_key+`')}}">`+this.product_name+`</a></h2>`+allergy_html+`
                            </div>
                        </div>
                    </li>
                
          `;
          });
//          html += `</ul>
//                <footer class="text-center">
//                    <a class="btn button1 trigger-order">Order now</a>
//                </footer>
//            </div>`;
          $('#myTabContent').html(html);
        }
      });
      
    } 
        var flag = true;
        setTimeout(function() {
            var categoryfromurl =  window.location.hash.substr(1);

            if(categoryfromurl !="" && flag == true){
                datakey = 'data-key='+categoryfromurl;
                $("["+datakey+"]").trigger("click");
                flag=false; 
            }
        }, 500);  
  });
</script>
@endpush -->
