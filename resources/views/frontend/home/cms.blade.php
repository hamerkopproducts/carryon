@extends('frontend.layouts.cmsdetail')

@section('content')
            <div id="content">
                <!-- section -->
                <div class="wrapper">
                    <div class="container">
                        {!! $content !!}
                    </div>
                </div>
            </div>
@endsection
