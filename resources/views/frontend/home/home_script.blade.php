<script type="text/javascript">
  $(document).ready(function(){

    @if(Session::has('message'))
        setTimeout(() => {
            alert("{{ Session::get('message') }}");
        }, 2000);
    @endif
    @if(Session::has('code'))
        if("{{ Session::get('code') }}" == 'PAYED_OK') {
            localStorage.setItem("flight", 'N/A');
            localStorage.setItem("flight_date", 'N/A');
            localStorage.setItem("can_order", false);
            localStorage.setItem("cart_key", '');
        }
    @endif

    var can_order = localStorage.getItem("can_order");
    var flight_date = localStorage.getItem("flight_date");
    var flight = localStorage.getItem("flight");
    if( can_order && ! $.inArray(flight, ['N/A', 'null', null, '']) !=-1) {

        var flight_updated_at = localStorage.getItem("flight_updated_at");
        if(flight_updated_at != null && new Date() - new Date(flight_updated_at) > 3600000) {
            localStorage.setItem("flight", 'N/A');
            localStorage.setItem("flight_date", 'N/A');
            localStorage.setItem("can_order", 0);
            localStorage.setItem("cart_key", "");
        }

        $('.flight_number_sidebar').html('<span>Flight number:</span>'+ flight );
        $('.flight_deliver_date_sidebar').html('<span>Delivery:</span>' + flight_date );
    }

    var url = $(location).attr('href'),
    parts = url.split("/"),
    last_part = parts[parts.length-1];

    if(last_part == ""){
        $('#menu_link').removeClass('current');
        $('#home_link').addClass('current');
        setTimeout(() => {
            $('#flight_popup').show();
        }, 50);
        setTimeout(() => {
            $('#flight_popup').show();
        }, 200);
    } else {
        $('#home_link').removeClass('current');
        $('#menu_link').addClass('current');
    }

    $('#flight_popup').on('click', 'button.close', function() {
        if(last_part == ""){
            // Display flight popup in home page even if its closed.
            $('#flight_popup').show();
        }
    });
    
    $('#logout').click(function(){
        $.ajax( {
        url: "{{url('/customer/logout')}}",
        dataType: "json",
        data: {
        },
        success: function( data ) {
                alert(data.message);
                localStorage.setItem("flight", 'N/A');
                localStorage.setItem("flight_date", 'N/A');
                localStorage.setItem("can_order", false);
                localStorage.setItem("cart_key", '');
                setTimeout(() => {
                    location.href = "<?php echo route('home_page')?>";
                }, 200);
            }
        });
    }); 


    @if( isset($categories) )

        @if(count($categories) > 0)
        getProducts('{{$categories[0]->id}}', 1);
        @endif

        $(".category").click(function(){
            getProducts($(this).attr("data-category"), $(this).attr("data-key"));
        });

        function getProducts(category_id, key){
        $.ajax({
            url: "{{url('/product/show')}}",
            dataType: "JSON",
            data: {
                category: category_id
            },
            success: function(data){
            var html = `<div class="tab-pane fade show active" id="tab`+key+`" role="tabpanel" aria-labelledby="tab`+key+`-tab">
                        <ul class="menu-list">`;
            $.each(data, function(){
                var allergy_html = '';
                allergy_html +=  `<footer>
                                        <ul class="flavour-list">`;
                $.each(this.allergies, function(){
                    allergy_html += `<li>
                                        <a data-toggle="tooltip" title="${this.description}">` +
                                            `<img src="{{asset('${this.image_path}')}}" alt="img">` +
                                        `</a>
                                    </li>`;
                });
                allergy_html += `</ul></footer>`;
                html +=
                `<li>
                    <div class="food">
                        <div class="image">
                            <a href="{{asset('/productdetail/${this.url_key}')}}"><img style="height:315px;" class="w-100" src="${this.product_image_path}"
                                    alt="img"></a>
                        </div>
                        <div class="info">
                            <h2><a href="{{asset('/productdetail/${this.url_key}')}}">`+this.product_name+`</a></h2>`+allergy_html+`
                        </div>
                    </div>
                </li>`;
            });
              html += `</ul>
                    <footer class="text-center">
                        <a class="btn button1 trigger-order">Order now</a>
                    </footer>
                </div>`;
            $('#myTabContent').html(html);
            }
        });
        
        } 
            var flag = true;
            setTimeout(function() {
                var categoryfromurl =  window.location.hash.substr(1);

                if(categoryfromurl !="" && flag == true){
                    datakey = 'data-key='+categoryfromurl;
                    $("["+datakey+"]").trigger("click");
                    flag=false; 
                }
            }, 500);  

    @endif
    
    
    function flightAdd() {
        if( localStorage.getItem('can_order') === null || localStorage.getItem('can_order') === 'null' ) {
            $('#flight_popup').show();
            $('.trigger-order').trigger('click');
            alert('Please add flight information');
            $('#basket button').trigger('click');
            return false;
        }

        if( localStorage.getItem('can_order') == false || localStorage.getItem('can_order') == 'false' ) {
            $('#flight_popup').show();
            $('.trigger-order').trigger('click');
            alert('Please update flight information');
            $('#basket button.toggle-btn').trigger('click');
            return false;
        }

        return true;
    }

    $(document).on('click', '.add-item-cart', function() {
        if( flightAdd() == false ) {
            return false;
        }
        var $qty = $('.add-item-cart').attr('item_count');
        var product_id = $(this).attr('product_id');
        var currentVal = parseInt($qty);
        if ( ! isNaN(currentVal) && $qty >= 0 ) {
            var newQty = currentVal + 1;
            addItemToCart(product_id, newQty, function() {
                $('.add-item-cart').attr('item_count', newQty);
            });
        }
    })

    function addItemToCart(product_id, newQty, callBack) {
        $('#overlay').show();
        $.ajax({
            url: "<?php echo route('cart_add')?>",
            method: "post",
            data: {
                productsid: product_id,
                item_count: newQty,
                key: localStorage.getItem('cart_key')
            },
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == true) {
                    if(typeof data.cartkey != 'undefined') {
                        localStorage.setItem("cart_key", data.cartkey);
                    }
                    callBack();
                    cart_list_side_view();
                    alert('Your cart updated');
                    $('.add-item-cart').attr('item_count', newQty);
                } else {
                    alert(data.message);
                }
            },
            complete: function() {
                $('#overlay').hide();
            }
        });
    }

    function cart_list_side_view() {
        $('#overlay').show();
        $('.pay_proceed').addClass('disabled');
        $.ajax({
            url: "<?php echo route('cart_list')?>",
            method: "post",
            data: {
                key: localStorage.getItem('cart_key')
            },
            success: function(data) {
                data = JSON.parse(data);
                var sideon_cart_string = '';
                if(data.status == true) {
                    for(var inc=0; inc < data.eachdata.length; inc++) {
                        sideon_cart_string += sideon_cart_list(
                            data.eachdata[inc].product_id, 
                            data.eachdata[inc].product_name, 
                            data.eachdata[inc].item_count_item_total, 
                            data.eachdata[inc].grant_total_item_total,
                            data.eachdata[inc].image_path
                        );
                    }
                    $('.sideon-cart-list').html(sideon_cart_string);
                    if(data.eachdata.length == 0) {
                        $('.pay_proceed').addClass('disabled');
                        var url = $(location).attr('href'),
                        parts = url.split("/"),
                        last_part = parts[parts.length-1]; 
                        if(last_part == "checkout" || last_part == "payment" || last_part == "billingaddress"){
                            setTimeout(() => {
                                location.href = "<?php echo route('our_menu') ?>";
                            }, 2000);
                            // if no items redirect to menu.
                            alert('No items in cart');
                        }
                        $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                        $('.sub_total').text('0.00');
                        $('.tax_total').text('0.00');
                        $('.grand_total').text('AED 0');
                        $('.total_cart_item_count').text(0);
                    } else {
                        $('.pay_proceed').removeClass('disabled');
                        $('.sub_total').text(data.maindata.sub_total_total);
                        $('.tax_total').text(data.maindata.tax_total_total);
                        grant_total_item_total = parseFloat(data.maindata.grant_total_total);
                        $('.grand_total').html( '<span>AED</span>' + grant_total_item_total );
                        $('.total_cart_item_count').text(data.maindata.item_count_total);
                    }
                } else {
                    $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                    if(data.message != 'Key is not passed')
                        alert(data.message);
                }
            },
            complete: function() {
                $('#overlay').hide();
            }
        });
    }

    function sideon_cart_list(product_id, product_name, item_count_item_total, grant_total_item_total, path) {
        grant_total_item_total = parseFloat(grant_total_item_total);
        return `<tr>
        <td class="product-image">
        <figure class="image">
        <img class="w-100" src="${path}" alt="img">
        </figure>
        <ul class="quantity">
        <li><button product_id=${product_id} type="button" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
        <li><input product_id=${product_id} type="number" class="input-control" value="${item_count_item_total}"></li>
        <li><button product_id=${product_id} type="button" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
        </ul>
        </td>
        <td class="product-details">
        <h4>${product_name}
        <small></small></h4>
        </td>
        <td class="product-price">
        AED <strong>${grant_total_item_total}</strong>
        </td>
        </tr>`;
    }

    $(document).on('click', '.add', function () {
        if( flightAdd() == false ) {
            return false;
        }

        var $qty = $(this).parents('.quantity').find('.input-control');
        var product_id = $(this).attr('product_id');
        var currentVal = parseInt($qty.val());
        if (!isNaN(currentVal)) {
            var newQty = currentVal + 1;
            addItemToCart(product_id, newQty, function() {
                $('.product_id_' + product_id).find('input.input-control').val(currentVal + 1);
                $('.product_id_' + product_id).removeClass('active');
                if(currentVal + 1 > 0) {
                    $('.product_id_' + product_id).addClass('active');
                }
            })
        }
    });

    $(document).on('click', '.less', function () {
        if( flightAdd() == false ) {
            return false;
        }

        var $qty = $(this).parents('.quantity').find('.input-control');
        var product_id = $(this).attr('product_id');
        var currentVal = parseInt($qty.val());
        if (!isNaN(currentVal) && currentVal > 0) {
            var newQty = currentVal - 1;
            addItemToCart(product_id, newQty, function() {
                $('.product_id_' + product_id).find('input.input-control').val(currentVal - 1);
                $('.product_id_' + product_id).removeClass('active');
                if(currentVal - 1 > 0) {
                    $('.product_id_' + product_id).addClass('active');
                }
            });
        }
    });

    $(document).on('focusout', '.quantity .input-control', function () {
        if( flightAdd() == false ) {
            return false;
        }
        var $qty = $(this);
        var product_id = $(this).attr('product_id');
        var currentVal = parseInt($qty.val());
        if (!isNaN(currentVal) && currentVal > 0) {
            var newQty = currentVal;
            addItemToCart(product_id, newQty, function() {
                $('.product_id_' + product_id).find('input.input-control').val(currentVal);
                $('.product_id_' + product_id).removeClass('active');
                if(currentVal > 0) {
                    $('.product_id_' + product_id).addClass('active');
                }
            });
        }
    });

    var timeoutInMiliseconds = 3600000;
    var timeoutId; 
    function startTimer() {
        timeoutId = window.setTimeout(doInactive, timeoutInMiliseconds)
    }
    function doInactive() {
        console.log('cleared');
        localStorage.setItem("can_order", 0);
        localStorage.setItem("flight", 'N/A');
        localStorage.setItem("flight_date", 'N/A');
    }
    function resetTimer() {
        window.clearTimeout(timeoutId)
        startTimer();
    }
    function setupTimers () {
        document.addEventListener("mousemove", resetTimer, false);
        document.addEventListener("mousedown", resetTimer, false);
        document.addEventListener("keypress", resetTimer, false);
        document.addEventListener("touchmove", resetTimer, false);
        startTimer();
    }
    setupTimers();
});
    
</script>