<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
</head>
<body>
    <h2>Demo mail from JustLaravel</h2>
@if(isset($bodyMessage))
    <div class="w3-container w3-orange">
<p>
{!! $bodyMessage !!} 
</p>
</div>
    @endif
</body>
</html>