@extends('admin.layouts.app')

@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card list-block">
                                <div class="card-title">
                                    <h4>Customer List</h4>
                                </div>
                                <div class="invoice-edit">
                                    
                                    <!--.row-->
                                    <div class="invoicelist-body">
                                        <div class="bootstrap-data-table-panel">
                                            <div class="table-responsive">
                                                <table  id="bootstrap-data-table" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                        <th>No</th>
                                                          <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            <th>Nationality</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if (count($customer_details) > 0)
                                                        @php
                                                        $count = 1;
                                                        @endphp
                                                        @foreach ($customer_details as $details)
                                                                    <tr>
                                                                    <td>{{$count++}}</td>
                                                                    <td>{{$details->name}}</td>
                                                                    <td>{{$details->email}}</td>
                                                                    <td>{{$details->phone}}</td>
                                                                    <td>{{$details->nationality}}</td>
                                                                    </tr>
                                                    @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="4" class="text-center"> No Categories Available</td>
                                                        </tr>
                                                    @endif  
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- <a class="control newRow add-new-item" href="#">+ Add Category</a> -->
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2018 © Admin Board.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection
   