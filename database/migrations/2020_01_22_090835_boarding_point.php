<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BoardingPoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boarding_point', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->timestamps();
        });

        Schema::create('pickup_point', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('boarding_point_id')->unsigned()->nullable();
            $table->string('name', 100);
            $table->timestamps();

            $table->index(['boarding_point_id']);
        });

        Schema::table('pickup_point', function (Blueprint $table) {
            $table->foreign('boarding_point_id')->references('id')->on('boarding_point')->onDelete('set null');
        });

        Schema::table('flights', function (Blueprint $table) {
            $table->bigInteger('boarding_point_id')->unsigned()->nullable();

            $table->index(['boarding_point_id']);
        });

        Schema::table('flights', function (Blueprint $table) {
            $table->foreign('boarding_point_id')->references('id')->on('boarding_point')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
