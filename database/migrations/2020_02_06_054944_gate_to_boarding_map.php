<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GateToBoardingMap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boarding_pickup_map', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('terminal');
            $table->string('gate', 200);
            $table->string('pickup_point', 200);
            $table->string('default', 200);
            $table->string('map_page_param', 512);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::table('order', function (Blueprint $table) {
            $table->string('gate', 200);
            $table->string('pickup_point', 200);
            $table->time('duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
