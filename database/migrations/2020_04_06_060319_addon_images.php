<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddonImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addon_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('addons_id')->unsigned();
            $table->text('path')->nullable();
            $table->enum('type', ['THUMBNAIL', 'IMAGE'])->default('IMAGE');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['addons_id']);
        });

        Schema::table('addon_images', function (Blueprint $table) {
            $table->foreign('addons_id')->references('id')->on('addons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
