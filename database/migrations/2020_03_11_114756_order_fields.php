<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function ($table) {
            $table->string('freshness', 10)->default('');
            $table->string('taste', 10)->default('');
            $table->string('variety', 10)->default('');
            $table->string('ordering_process', 10)->default('');
            $table->text('comment')->nullable();
            $table->text('key')->nullable();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
