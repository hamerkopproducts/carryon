<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddonsCartItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addons_cart_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_items_id')->unsigned();
            $table->bigInteger('addons_id')->unsigned();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['cart_items_id']);
            $table->index(['addons_id']);
        });

        Schema::table('addons_cart_items', function (Blueprint $table) {
            $table->foreign('cart_items_id')->references('id')->on('cart_items')->onDelete('cascade');
            $table->foreign('addons_id')->references('id')->on('addons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
