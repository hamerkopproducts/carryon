<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('description', 100);
            $table->enum('visibility', ['YES', 'NO'])->default('NO');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::create('allergies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('description', 100);
            $table->text('image_path')->nullable();
            $table->enum('visibility', ['YES', 'NO'])->default('NO');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::create('incredience', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('description', 100);
            $table->text('image_path')->nullable();
            $table->enum('visibility', ['YES', 'NO'])->default('NO');
            $table->enum('is_filter', ['YES', 'NO'])->default('NO');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::create('flights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('flight_no', 15);
            $table->string('destination', 100);
            $table->string('departure', 45);
            $table->string('duration', 45);
            $table->timestamps();
        });

        Schema::create('cms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url_key', 100)->nullable();
            $table->text('html_content')->nullable();
            $table->string('page_title', 200)->nullable();
            $table->text('meta_data')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->enum('visibility', ['YES', 'NO'])->default('NO');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('sku', 200);
            $table->unsignedDecimal('actual_price', 10, 2);
            $table->unsignedDecimal('discount_price', 10, 2)->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::create('product_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('products_id')->unsigned();
            $table->text('path')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['products_id']);
        });

        Schema::table('product_images', function (Blueprint $table) {
            $table->foreign('products_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('product_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('products_id')->unsigned()->nullable();
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['products_id', 'category_id']);
        });

        Schema::table('product_category', function (Blueprint $table) {
            $table->foreign('products_id')->references('id')->on('products')->onDelete('set null');
            $table->foreign('category_id')->references('id')->on('category')->onDelete('set null');
        });

        Schema::create('product_allergies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('products_id')->unsigned()->nullable();
            $table->bigInteger('allergies_id')->unsigned()->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['products_id', 'allergies_id']);
        });

        Schema::table('product_allergies', function (Blueprint $table) {
            $table->foreign('products_id')->references('id')->on('products')->onDelete('set null');
            $table->foreign('allergies_id')->references('id')->on('allergies')->onDelete('set null');
        });

        Schema::create('product_incredience', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('products_id')->unsigned()->nullable();
            $table->bigInteger('incredience_id')->unsigned()->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['products_id', 'incredience_id']);
        });

        Schema::table('product_incredience', function (Blueprint $table) {
            $table->foreign('products_id')->references('id')->on('products')->onDelete('set null');
            $table->foreign('incredience_id')->references('id')->on('incredience')->onDelete('set null');
        });

        Schema::create('cart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('users_id')->nullable();
            $table->integer('item_count')->nullable();
            $table->unsignedDecimal('grant_total', 10, 2);
            $table->unsignedDecimal('sub_total', 10, 2);
            $table->unsignedDecimal('tax_total', 10, 2);
            $table->enum('is_guest', ['YES', 'NO'])->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::create('cart_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_id')->unsigned()->nullable();
            $table->bigInteger('products_id')->unsigned()->nullable();
            $table->integer('item_count');
            $table->unsignedDecimal('grant_total', 10, 2);
            $table->unsignedDecimal('sub_total', 10, 2);
            $table->unsignedDecimal('tax_total', 10, 2);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['products_id', 'cart_id']);
        });

        Schema::table('cart_items', function (Blueprint $table) {
            $table->foreign('products_id')->references('id')->on('products')->onDelete('set null');
            $table->foreign('cart_id')->references('id')->on('cart')->onDelete('set null');
        });

        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('users_id')->unsigned()->nullable();
            $table->integer('item_count')->nullable();
            $table->unsignedDecimal('grant_total', 10, 2);
            $table->unsignedDecimal('sub_total', 10, 2);
            $table->unsignedDecimal('tax_total', 10, 2);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['users_id']);
        });

        Schema::table('order', function (Blueprint $table) {
            $table->foreign('users_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned()->nullable();
            $table->bigInteger('products_id')->unsigned()->nullable();
            $table->integer('item_count')->nullable();
            $table->unsignedDecimal('grant_total', 10, 2);
            $table->unsignedDecimal('sub_total', 10, 2);
            $table->unsignedDecimal('tax_total', 10, 2);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['products_id', 'order_id']);
        });

        Schema::table('order_items', function (Blueprint $table) {
            $table->foreign('products_id')->references('id')->on('products')->onDelete('set null');
            $table->foreign('order_id')->references('id')->on('order')->onDelete('set null');
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned()->nullable();
            $table->bigInteger('order_users_id');
            $table->enum('status', ['INITIATED', 'CANCELLED', 'FAILED', 'SUCCESS'])->default('INITIATED');
            $table->string('receipt', 512);
            $table->float('amount', 10, 2);
            $table->string('signature', 512);
            $table->string('reference_id', 512);
            $table->string('payment_mode', 512);
            $table->enum('type', ['ONLINE', 'OFFLINE', 'INIT'])->default('INIT');
            $table->integer('note')->nullable();
            $table->timestamps();

            $table->index(['order_id']);
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('order')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category');
        Schema::drop('allergies');
        Schema::drop('incredience');
        Schema::drop('flights');
        Schema::drop('cms');
        Schema::drop('products');
        Schema::drop('product_images');
        Schema::drop('product_category');
        Schema::drop('product_allergies');
        Schema::drop('product_incredience');
        Schema::drop('cart');
        Schema::drop('cart_items');
        Schema::drop('order');
        Schema::drop('payments');

    }
}
