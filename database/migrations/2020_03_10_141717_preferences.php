<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Preferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('pref_age_group', 512)->default('');
            $table->string('pref_gender', 512)->default('');
            $table->string('pref_country', 512)->default('');
            $table->string('pref_intolerances', 512)->default('');
            $table->string('pref_diet', 512)->default('');
            $table->string('pref_lifestyle', 512)->default('');
            $table->string('pref_travel_pattern', 512)->default('');
            $table->string('pref_travel_for', 512)->default('');
        });        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
